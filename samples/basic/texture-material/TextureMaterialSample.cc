#include "TextureMaterialSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/ColorSpace.hh>
#include <glow/data/TextureData.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/geometry/UVSphere.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void TextureMaterialSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShaderBG = Program::createFromFile(util::pathOf(__FILE__) + "/bg");
    mShaderLight = Program::createFromFile(util::pathOf(__FILE__) + "/light");
    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    auto cube = geometry::Cube<>().generate();
    mCube = VertexArray::create(cube->getAttributeBuffer("aPosition"), GL_PATCHES);
    mCube->setVerticesPerPatch(4);
    mQuad = geometry::Quad<>().generate();
    mSphere = geometry::UVSphere<>().generate();

    mTexAlbedo = Texture2D::createFromFile(util::pathOf(__FILE__) + "/TexturesCom_BareThreadplateFloor_1024_albedo.png",
                                           ColorSpace::sRGB);
    mTexAO = Texture2D::createFromFile(util::pathOf(__FILE__) + "/TexturesCom_BareThreadplateFloor_1024_ao.png", ColorSpace::Linear);
    mTexHeight = Texture2D::createFromFile(util::pathOf(__FILE__) + "/TexturesCom_BareThreadplateFloor_1024_height.png",
                                           ColorSpace::Linear);
    mTexMetallic = Texture2D::createFromFile(
        util::pathOf(__FILE__) + "/TexturesCom_BareThreadplateFloor_1024_metallic.png", ColorSpace::Linear);
    mTexNormal = Texture2D::createFromFile(util::pathOf(__FILE__) + "/TexturesCom_BareThreadplateFloor_1024_normal.png",
                                           ColorSpace::Linear);
    mTexRoughness = Texture2D::createFromFile(
        util::pathOf(__FILE__) + "/TexturesCom_BareThreadplateFloor_1024_roughness.png", ColorSpace::Linear);

    auto pbt = util::pathOf(__FILE__) + "/../../../data/cubemap/miramar";
    mCubeMap = TextureCubeMap::createFromData(TextureData::createFromFileCube( //
        pbt + "/posx.jpg",                                                     //
        pbt + "/negx.jpg",                                                     //
        pbt + "/posy.jpg",                                                     //
        pbt + "/negy.jpg",                                                     //
        pbt + "/posz.jpg",                                                     //
        pbt + "/negz.jpg",                                                     //
        ColorSpace::sRGB));

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});
    cam->setNearPlane(0.001f);

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");

    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "group=light ");
    TwAddVarRW(tweakbar(), "Light Dis", TW_TYPE_FLOAT, &mLightDis, "group=light step=0.01 min=1.0 max=100.0");
    TwAddVarRW(tweakbar(), "Light Radius", TW_TYPE_FLOAT, &mLightRadius, "group=light step=0.01 min=0.01 max=10.0");

    TwAddVarRW(tweakbar(), "Use Albedo", TW_TYPE_BOOLCPP, &mUseAlbedo, "group=material ");
    TwAddVarRW(tweakbar(), "Use AO", TW_TYPE_BOOLCPP, &mUseAO, "group=material ");
    TwAddVarRW(tweakbar(), "Use Height", TW_TYPE_BOOLCPP, &mUseHeight, "group=material ");
    TwAddVarRW(tweakbar(), "Use Normal", TW_TYPE_BOOLCPP, &mUseNormal, "group=material ");
    TwAddVarRW(tweakbar(), "Use Metallic", TW_TYPE_BOOLCPP, &mUseMetallic, "group=material ");
    TwAddVarRW(tweakbar(), "Use Roughness", TW_TYPE_BOOLCPP, &mUseRoughness, "group=material ");
    TwAddVarRW(tweakbar(), "Use Reflection", TW_TYPE_BOOLCPP, &mUseReflection, "group=material ");
    TwAddVarRW(tweakbar(), "Show Texture #", TW_TYPE_INT32, &mShowTexture, "group=debug min=-1 max=6 ");

    TwDefine("Tweakbar size='300 400' valueswidth=140");
}

void TextureMaterialSample::render(float elapsedSeconds)
{
    if (mAnimate)
        mRuntime += elapsedSeconds;

    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0.00f, 0.33f, 0.66f, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    { // bg
        auto shader = mShaderBG->use();
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
        shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
        shader.setTexture("uTexture", mCubeMap);

        mQuad->bind().draw();
    }

    { // scene
        GLOW_SCOPED(enable, GL_CULL_FACE);

        auto shader = mShader->use();
        shader.setUniform("uView", cam->getViewMatrix());
        shader.setUniform("uProj", cam->getProjectionMatrix());
        shader.setUniform("uModel", glm::mat4());
        shader.setUniform("uCamPos", cam->getPosition());

        // textures
        shader.setTexture("uCubeMap", mCubeMap);
        shader.setTexture("uTexAlbedo", mTexAlbedo);
        shader.setTexture("uTexAO", mTexAO);
        shader.setTexture("uTexHeight", mTexHeight);
        shader.setTexture("uTexNormal", mTexNormal);
        shader.setTexture("uTexRoughness", mTexRoughness);
        shader.setTexture("uTexMetallic", mTexMetallic);
        shader.setUniform("uShowTexture", mShowTexture);

        // displacement mapping
        shader.setUniform("uDisplacement", 0.05f);
        shader.setUniform("uTessellationLevel", 64.0f);

        // shading
        shader.setUniform("uUseAlbedo", mUseAlbedo);
        shader.setUniform("uUseAO", mUseAO);
        shader.setUniform("uUseHeight", mUseHeight);
        shader.setUniform("uUseNormal", mUseNormal);
        shader.setUniform("uUseMetallic", mUseMetallic);
        shader.setUniform("uUseRoughness", mUseRoughness);
        shader.setUniform("uUseReflection", mUseReflection);

        // lights
        shader.setUniform("uLightRadius", mLightRadius);
        shader.setUniform("uLightPos", mLightDis * normalize(mLightDir));

        shader.setUniform("uModel", rotate(mRuntime, glm::vec3(0, 1, 0)) * translate(glm::vec3(0, 0, 0)));
        mCube->bind().draw();
    }

    // light
    {
        auto shader = mShaderLight->use();
        shader.setUniform("uView", cam->getViewMatrix());
        shader.setUniform("uProj", cam->getProjectionMatrix());
        shader.setUniform("uModel", translate(mLightDis * normalize(mLightDir)) * scale(glm::vec3(mLightRadius)));
        mSphere->bind().draw();
    }
}
