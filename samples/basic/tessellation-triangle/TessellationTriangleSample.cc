#include "TessellationTriangleSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/ColorSpace.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void TessellationTriangleSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");

    std::vector<glm::vec3> vertices;
    vertices.push_back({-0.8, -0.8, 0.0});
    vertices.push_back({+0.8, -0.8, 0.0});
    vertices.push_back({0.0, 0.8, 0.0});
    auto ab = ArrayBuffer::create();
    ab->defineAttribute<glm::vec3>("aPosition");
    ab->bind().setData(vertices);
    mTriangle = VertexArray::create(ab, GL_PATCHES);
    mTriangle->setVerticesPerPatch(3);

    TwAddVarRW(tweakbar(), "Inner Level", TW_TYPE_FLOAT, &mInnerLvl, "step=0.1 min=1.0 max=64.0");
    TwAddVarRW(tweakbar(), "Outer Level A", TW_TYPE_FLOAT, &mOuterLvlA, "step=0.1 min=1.0 max=64.0");
    TwAddVarRW(tweakbar(), "Outer Level B", TW_TYPE_FLOAT, &mOuterLvlB, "step=0.1 min=1.0 max=64.0");
    TwAddVarRW(tweakbar(), "Outer Level C", TW_TYPE_FLOAT, &mOuterLvlC, "step=0.1 min=1.0 max=64.0");

    // TwDefine("Tweakbar size='300 400' valueswidth=140");
}

void TessellationTriangleSample::render(float elapsedSeconds)
{
    GLOW_SCOPED(clearColor, 0.00, 0.33, 0.66, 1);
    // GLOW_SCOPED(clearColor, 1,1,1, 1);
    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);
    glClear(GL_COLOR_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uInnerLvl", mInnerLvl);
    shader.setUniform("uOuterLvlA", mOuterLvlA);
    shader.setUniform("uOuterLvlB", mOuterLvlB);
    shader.setUniform("uOuterLvlC", mOuterLvlC);

    GLOW_SCOPED(polygonMode, GL_LINE);

    mTriangle->bind().draw();
}
