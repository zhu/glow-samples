#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class TessellationTriangleSample : public glow::glfw::GlfwApp
{
public:
    TessellationTriangleSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mTriangle;

    float mOuterLvlA = 1;
    float mOuterLvlB = 1;
    float mOuterLvlC = 1;
    float mInnerLvl = 1;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
