#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

enum class BlendMode
{
    MacroOnly,
    DetailOnly,
    Linear,
    PartialDeriv,
    Whiteout,
    RNM
};

class NormalBlendingSample : public glow::glfw::GlfwApp
{
public:
    NormalBlendingSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mCube;

    glow::SharedTexture2D mNormalsBase;
    glow::SharedTexture2D mNormalsDetail;

    glm::vec3 mLightDir = normalize(glm::vec3(-.2, 1, .2));
    BlendMode mMode = BlendMode::RNM;

    bool mShowNormals = false;
    bool mSwitchMaps = false;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
