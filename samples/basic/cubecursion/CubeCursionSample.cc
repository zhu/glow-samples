#include "CubeCursionSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Texture2D.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void CubeCursionSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

	mTexColor = Texture2D::create(mTexSize, mTexSize, GL_RGB);
	mTexDepth = Texture2D::create(mTexSize, mTexSize, GL_DEPTH_COMPONENT32);
	mFramebuffer = Framebuffer::create({ {"fColor", mTexColor} }, mTexDepth);

	mTexColor->bind().generateMipmaps(); // to silence first warning

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mCube = geometry::Cube<>().generate();

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");
}

void CubeCursionSample::render(float elapsedSeconds)
{
    mRuntime += elapsedSeconds;
    auto time = mAnimate ? mRuntime : 0.0f;

    auto cam = getCamera();

    GLOW_SCOPED(enable, GL_DEPTH_TEST);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", time);
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uModel", glm::rotate(time, glm::vec3{0, 1, 0}));
	shader.setTexture("uTexture", mTexColor);

	// draw to fb
	{
		auto fb = mFramebuffer->bind();

		GLOW_SCOPED(clearColor, 0.00, 0.33, 0.66, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shader.setUniform("uDrawColors", true);
		mCube->bind().draw();
	}
	mTexColor->bind().generateMipmaps();

	// draw to screen
	GLOW_SCOPED(clearColor, 0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shader.setUniform("uDrawColors", false);
	mCube->bind().draw();
}

