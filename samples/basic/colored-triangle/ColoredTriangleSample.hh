#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class ColoredTriangleSample : public glow::glfw::GlfwApp
{
public:
    ColoredTriangleSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mTriangle;

    float mRuntime = 0.0f;
    bool mAnimate = true;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
