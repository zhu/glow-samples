#include "LightingSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/ColorSpace.hh>
#include <glow/data/TextureData.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/geometry/UVSphere.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void LightingSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mShaderBG = Program::createFromFile(util::pathOf(__FILE__) + "/bg");
    mSphere = geometry::UVSphere<>().generate();
    mPlane = geometry::Quad<geometry::CubeVertex>().generate([](float u, float v) {
        return geometry::CubeVertex{glm::vec3((u * 2 - 1) * 10000, 0, (v * 2 - 1) * 10000), //
                                    glm::vec3(0, 1, 0),                                     //
                                    glm::vec3(1, 0, 0),                                     //
                                    glm::vec2(u, v)};
    });
    mQuad = geometry::Quad<>().generate();

    auto pbt = util::pathOf(__FILE__) + "/../../../data/cubemap/miramar";
    mCubeMap = TextureCubeMap::createFromData(TextureData::createFromFileCube( //
        pbt + "/posx.jpg",                                                     //
        pbt + "/negx.jpg",                                                     //
        pbt + "/posy.jpg",                                                     //
        pbt + "/negy.jpg",                                                     //
        pbt + "/posz.jpg",                                                     //
        pbt + "/negz.jpg",                                                     //
        ColorSpace::sRGB));

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});
    cam->setNearPlane(0.001f);


    TwEnumVal lightSrcEV[] = {
        {(int)LightSource::Directional, "Directional"}, //
        {(int)LightSource::Point, "Point"},             //
        {(int)LightSource::Sphere, "Sphere"},           //
    };
    auto lightSrcType = TwDefineEnum("Light Source", lightSrcEV, 3);
    TwAddVarRW(tweakbar(), "Light Source", lightSrcType, &mLightSource, "group=light");

    TwAddVarRW(tweakbar(), "Ambient Light", TW_TYPE_COLOR3F, &mAmbient, "group=light");
    TwAddVarRW(tweakbar(), "Light Color", TW_TYPE_COLOR3F, &mLightColor, "group=light ");
    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "group=light ");
    TwAddVarRW(tweakbar(), "Light Dis", TW_TYPE_FLOAT, &mLightDis, "group=light step=0.01 min=1.0 max=100.0");
    TwAddVarRW(tweakbar(), "Sphere Radius", TW_TYPE_FLOAT, &mLightRadius, "group=light step=0.01 min=0.01 max=100.0");
    TwAddVarRW(tweakbar(), "Spot Light", TW_TYPE_BOOLCPP, &mSpotLight, "group=light");
    TwAddVarRW(tweakbar(), "Spot Target", TW_TYPE_DIR3F, &mLightTarget, "group=light ");
    TwAddVarRW(tweakbar(), "Spot Inner Angle", TW_TYPE_FLOAT, &mLightInnerAngle, "group=light step=1 min=0.0 max=170.0");
    TwAddVarRW(tweakbar(), "Spot Outer Angle", TW_TYPE_FLOAT, &mLightOuterAngle, "group=light step=1 min=0.0 max=170.0");
    TwAddVarRW(tweakbar(), "Attenuation Constant", TW_TYPE_FLOAT, &mAttenuationConstant, "group=light step=0.1 min=0.0 max=1.0");
    TwAddVarRW(tweakbar(), "Attenuation Linear", TW_TYPE_FLOAT, &mAttenuationLinear, "group=light step=0.01 min=0.0 max=10.0");
    TwAddVarRW(tweakbar(), "Attenuation Quadratic", TW_TYPE_FLOAT, &mAttenuationQuadratic, "group=light step=0.01 min=0.0 max=10.0");


    TwEnumVal shadingDiffuseEV[] = {
        {(int)ShadingDiffuse::None, "None"},            //
        {(int)ShadingDiffuse::Lambert, "Lambert"},      //
        {(int)ShadingDiffuse::OrenNayar, "Oren-Nayar"}, //
    };
    auto shadingDiffuseType = TwDefineEnum("ShadingDiffuse", shadingDiffuseEV, 3);
    TwAddVarRW(tweakbar(), "ShadingDiffuse", shadingDiffuseType, &mShadingDiffuse, "group=shading");

    TwEnumVal shadingSpecularEV[] = {
        {(int)ShadingSpecular::None, "None"},                     //
        {(int)ShadingSpecular::Phong, "Phong"},                   //
        {(int)ShadingSpecular::BlinnPhong, "Blinn-Phong"},        //
        {(int)ShadingSpecular::CookTorrance, "Cook-Torrance"},    //
        {(int)ShadingSpecular::WardAnisotropic, "Ward (aniso.)"}, //
        {(int)ShadingSpecular::GGX, "GGX"},                       //
    };
    auto shadingSpecularType = TwDefineEnum("ShadingSpecular", shadingSpecularEV, 6);
    TwAddVarRW(tweakbar(), "ShadingSpecular", shadingSpecularType, &mShadingSpecular, "group=shading");

    TwAddVarRW(tweakbar(), "Diffuse", TW_TYPE_COLOR3F, &mDiffuse, "group=shading");
    TwAddVarRW(tweakbar(), "Specular", TW_TYPE_COLOR3F, &mSpecular, "group=shading");
    TwAddVarRW(tweakbar(), "Shininess", TW_TYPE_FLOAT, &mShininess, "group=shading step=0.1 min=0.1 max=128.0");
    TwAddVarRW(tweakbar(), "Roughness", TW_TYPE_FLOAT, &mRoughness, "group=shading step=0.01 min=0.01 max=1.0");
    TwAddVarRW(tweakbar(), "Aniso X", TW_TYPE_FLOAT, &mAnisoX, "group=shading step=0.1 min=0.1 max=5.0");
    TwAddVarRW(tweakbar(), "Aniso Y", TW_TYPE_FLOAT, &mAnisoY, "group=shading step=0.1 min=0.1 max=5.0");
    TwAddVarRW(tweakbar(), "Cube LOD", TW_TYPE_FLOAT, &mCubeLOD, "group=shading step=0.1 min=0.0 max=10.0");
    TwAddVarRW(tweakbar(), "Reflectivity", TW_TYPE_FLOAT, &mReflectivity, "group=shading step=0.01 min=0.0 max=1.0");

    TwDefine("Tweakbar size='300 750' valueswidth=140");
}

void LightingSample::render(float elapsedSeconds)
{
    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0.00, 0.33, 0.66, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    { // bg
        auto shader = mShaderBG->use();
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
        shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
        shader.setTexture("uTexture", mCubeMap);

        mQuad->bind().draw();
    }

    { // scene
        GLOW_SCOPED(enable, GL_CULL_FACE);

        auto shader = mShader->use();
        shader.setUniform("uView", cam->getViewMatrix());
        shader.setUniform("uProj", cam->getProjectionMatrix());
        shader.setUniform("uModel", glm::mat4());
        shader.setUniform("uCamPos", cam->getPosition());
        shader.setTexture("uCubeMap", mCubeMap);

        // shading
        shader.setUniform("uShadingDiffuse", (int)mShadingDiffuse);
        shader.setUniform("uShadingSpecular", (int)mShadingSpecular);
        shader.setUniform("uDiffuse", mDiffuse);
        shader.setUniform("uSpecular", mSpecular);
        shader.setUniform("uShininess", mShininess);
        shader.setUniform("uReflectivity", mReflectivity);
        shader.setUniform("uRoughness", mRoughness);
        shader.setUniform("uAnisoX", mAnisoX);
        shader.setUniform("uAnisoY", mAnisoY);
        shader.setUniform("uCubeLOD", mCubeLOD);

        // lights
        shader.setUniform("uLightSource", (int)mLightSource);
        shader.setUniform("uLightDir", mLightDir);
        shader.setUniform("uSpotLight", mSpotLight);
        shader.setUniform("uLightTarget", mLightTarget);
        shader.setUniform("uLightRadius", mLightRadius);
        shader.setUniform("uLightColor", mLightColor);
        shader.setUniform("uAmbientLight", mAmbient);
        shader.setUniform("uLightPos", mLightDis * normalize(mLightDir));
        shader.setUniform("uLightInnerCos", glm::acos(glm::radians(mLightInnerAngle)));
        shader.setUniform("uLightOuterCos", glm::acos(glm::radians(mLightOuterAngle)));
        shader.setUniform("uAttenuationConstant", mAttenuationConstant);
        shader.setUniform("uAttenuationLinear", mAttenuationLinear);
        shader.setUniform("uAttenuationQuadratic", mAttenuationQuadratic);

        shader.setUniform("uModel", translate(glm::vec3(0, -2, 0)));
        mPlane->bind().draw();
        shader.setUniform("uModel", translate(glm::vec3(0, 0, 0)));
        mSphere->bind().draw();

        // light
        if (mLightSource != LightSource::Directional)
        {
            auto lightRad = mLightSource == LightSource::Sphere ? mLightRadius : 0.1f;
            shader.setUniform("uModel", translate(mLightDis * normalize(mLightDir)) * scale(glm::vec3(lightRad)));
            shader.setUniform("uShadingDiffuse", (int)ShadingDiffuse::None);
            shader.setUniform("uShadingSpecular", (int)ShadingSpecular::None);
            shader.setUniform("uAmbientLight", glm::pow(mLightColor, glm::vec3(2.2)));
            shader.setUniform("uDiffuse", glm::vec3(1.0));
            mSphere->bind().draw();
        }
    }
}
