layout(quads, fractional_even_spacing, ccw) in;

uniform sampler2D uTexColor;
uniform sampler2D uTexNormal;
uniform sampler2D uTexHeight;

uniform mat4 uView;
uniform mat4 uProj;

uniform float uDisplacement;

in vec2 eTexCoord[];
in vec3 eNormal[];
in vec3 eTangent[];
in vec3 eWorldPos[];

out vec3 tNormal;
out vec3 tTangent;
out vec3 tWorldPos;
out vec2 tTexCoord;

vec3 bmix3(vec3 p0, vec3 p1, vec3 p2, vec3 p3)
{
    vec3 p01 = mix(p1, p0, gl_TessCoord.x);
    vec3 p23 = mix(p2, p3, gl_TessCoord.x);

    return mix(p01, p23, gl_TessCoord.y);
}
vec2 bmix2(vec2 p0, vec2 p1, vec2 p2, vec2 p3)
{
    vec2 p01 = mix(p1, p0, gl_TessCoord.x);
    vec2 p23 = mix(p2, p3, gl_TessCoord.x);

    return mix(p01, p23, gl_TessCoord.y);
}

void main()
{    
    tNormal = bmix3(eNormal[0], eNormal[1], eNormal[2], eNormal[3]);
    tTangent = bmix3(eTangent[0], eTangent[1], eTangent[2], eTangent[3]);
    tTexCoord = bmix2(eTexCoord[0], eTexCoord[1], eTexCoord[2], eTexCoord[3]);
    tWorldPos = bmix3(eWorldPos[0], eWorldPos[1], eWorldPos[2], eWorldPos[3]);

    float h = texture(uTexHeight, tTexCoord).r;
    float border = 0.02;
    h *= smoothstep(0.0, border, tTexCoord.x);
    h *= smoothstep(0.0, border, 1 - tTexCoord.x);
    h *= smoothstep(0.0, border, tTexCoord.y);
    h *= smoothstep(0.0, border, 1 - tTexCoord.y);
    tWorldPos += h * uDisplacement * tNormal;

    gl_Position = uProj * uView * vec4(tWorldPos, 1.0);
}