uniform sampler2D uTexColor;
uniform sampler2D uTexNormal;
uniform sampler2D uTexHeight;

uniform int uTechnique;
uniform float uApparentDepth;

uniform vec3 uLightPos;

uniform vec3 uCamPos;
uniform vec3 uCamUp;
uniform vec3 uCamRight;

uniform float uAmbient;
uniform float uDiffuse;
uniform float uSpecular;
uniform float uShininess;

uniform bool uShowNormals;
uniform bool uUseMicrofacetModel;
uniform bool uDeriveNormals;
uniform bool uDeriveTangents;

uniform int uParallaxSamples;

in vec2 tTexCoord;
in vec3 tNormal;
in vec3 tTangent;
in vec3 tWorldPos;

out vec3 fColor;

vec3 shadingDiffuseOrenNayar(vec3 N, vec3 V, vec3 L, float roughness, vec3 albedo) 
{
    float dotVL = dot(L, V);
    float dotLN = dot(L, N);
    float dotNV = dot(N, V);

    float s = dotVL - dotLN * dotNV;
    float t = mix(1.0, max(dotLN, dotNV), step(0.0, s));

    float sigma2 = roughness * roughness;
    vec3 A = 1.0 + sigma2 * (albedo / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));    
    float B = 0.45 * sigma2 / (sigma2 + 0.09);

    return albedo * max(0.0, dotLN) * (A + B * s / t);
}

// DO NOT MULTIPLY BY COS THETA
vec3 shadingSpecularGGX(vec3 N, vec3 V, vec3 L, float roughness, vec3 F0)
{
    // see http://www.filmicworlds.com/2014/04/21/optimizing-ggx-shaders-with-dotlh/
    vec3 H = normalize(V + L);

    float dotLH = max(dot(L, H), 0.0);
    float dotNH = max(dot(N, H), 0.0);
    float dotNL = max(dot(N, L), 0.0);
    float dotNV = max(dot(N, V), 0.0);

    float alpha = roughness * roughness;

    // D (GGX normal distribution)
    float alphaSqr = alpha * alpha;
    float denom = dotNH * dotNH * (alphaSqr - 1.0) + 1.0;
    float D = alphaSqr / (denom * denom);
    // no pi because BRDF -> lighting

    // F (Fresnel term)
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5); // manually?
    vec3 F = mix(vec3(F_b), vec3(F_a), F0);

    // G (remapped hotness, see Unreal Shading)
    float k = (alpha + 2 * roughness + 1) / 8.0;
    float G = dotNL / (mix(dotNL, 1, k) * mix(dotNV, 1, k));
    // '* dotNV' - canceled by normalization

    // '/ dotLN' - canceled by lambert
    // '/ dotNV' - canceled by G
    return D * F * G / 4.0;
}

vec3 phong(vec3 baseColor, vec3 N, vec3 V, vec3 L)
{
    vec3 color = vec3(0.0);

    vec3 R = reflect(-V, N);
    float dotNL = dot(N, L);
    float dotRL = dot(R, L);

    // ambient
    color += uAmbient * baseColor;

    // diffuse
    color += uDiffuse * baseColor * max(0.0, dotNL);

    // specular
    if (dotNL > 0)
        color += uSpecular * vec3(1,1,1) * pow(max(0.0, dotRL), uShininess);

    // gamma correction
    return pow(color, vec3(1/2.2));
}

vec3 shading(vec3 baseColor, vec3 N, vec3 V, vec3 L)
{
    // microfacets
    if (uUseMicrofacetModel)
    {
        vec3 color = uAmbient * baseColor;
        float metallic = 0.0;
        float roughness = 0.5;
        vec3 diffuse = baseColor * (1 - metallic); // metals have no diffuse
        vec3 specular = mix(vec3(0.04), baseColor, metallic); // fixed spec for non-metals
        vec3 lightColor = vec3(1.0);

        color += lightColor * shadingDiffuseOrenNayar(N, V, L, roughness, diffuse);
        color += lightColor * shadingSpecularGGX(N, V, L, max(0.01, roughness), specular);

        return color;
    }
    else // phong
    {    
        return phong(baseColor, N, V, L);
    }    
}

void main() 
{
    // calculate dirs
    vec3 N = normalize(tNormal);
    vec3 T = normalize(tTangent);
    vec3 L = normalize(uLightPos - tWorldPos);
    vec3 V = normalize(uCamPos - tWorldPos);
    vec3 B = normalize(cross(T, N));
    mat3 tbn = mat3(T, B, N);

    // dFdx normals
    if (uDeriveNormals)
    {
        N = normalize(cross(dFdx(tWorldPos), dFdy(tWorldPos)));
    }

    // dFdx tangents
    if (uDeriveTangents)
    {
        float u10 = dFdx(tTexCoord.x);
        float v10 = dFdx(tTexCoord.y);
        float u20 = dFdy(tTexCoord.x);
        float v20 = dFdy(tTexCoord.y);

        vec3 q1 = dFdx(tWorldPos);
        vec3 q2 = dFdy(tWorldPos);

        vec3 nom    = q2 - q1 * v20 / v10;
        float denom = u20 - u10 * v20 / v10;
        T = nom / denom;
        T -= N * dot(T, N);
        T = normalize(T);
        B = cross(T, N);
        
        tbn = mat3(T, B, N);
    }

    // parallax mapping
    vec2 texCoord = tTexCoord;
    if (uTechnique == 6)
    {
        vec3 texViewDir = -normalize(transpose(tbn) * V);
        float stepSize = 1.0 / uParallaxSamples * 0.2;
        vec3 texOffs = vec3(0.0);

        // linear search
        float lastT = 0.0;
        float currT = 0.0;
        for (int i = 0; i < uParallaxSamples; ++i) 
        {
            currT += stepSize;
            texOffs = texViewDir * currT;            

            float height = -(1-texture(uTexHeight, texCoord + texOffs.xy).x) * uApparentDepth;
            if (height > texOffs.z)
                break;

            lastT = currT;
        }

        // binary search
        float minT = lastT;
        float maxT = currT;
        for (int i = 0; i < 8; ++i)
        {
            float midT = (minT + maxT) / 2.0;
            texOffs = texViewDir * midT;
            
            float height = -(1-texture(uTexHeight, texCoord + texOffs.xy).x) * uApparentDepth;
            if (height > texOffs.z)
                maxT = midT;
            else 
                minT = midT;
        }

        texCoord += texOffs.xy;
    }

    // fetch textures
    vec3 colorMap = texture(uTexColor, texCoord).rgb;
    float heightMap = texture(uTexHeight, texCoord).r;
    vec3 normalMap = texture(uTexNormal, texCoord).rgb;
    normalMap.xy = normalMap.xy * 2 - 1; // project xy properly

    // phong only
    if (uTechnique == 0)
    {
        fColor = shading(colorMap, N, V, L);
    }
    // color only
    else if (uTechnique == 1)
    {
        fColor = pow(colorMap, vec3(1/2.2));
    }
    // normal only
    else if (uTechnique == 2)
    {
        fColor = normalMap;
    }
    // height only
    else if (uTechnique == 3)
    {
        fColor = heightMap.rrr;
    }
    // bump mapped
    else if (uTechnique == 4)
    {
        float f = heightMap * uApparentDepth;
        float epsX = length(dFdx(tWorldPos));
        float epsY = length(dFdy(tWorldPos));
        float f_x = dFdx(f) / epsX;
        float f_y = dFdy(f) / epsY;
        N = normalize(N - f_x * uCamRight - f_y * uCamUp);
        fColor = shading(colorMap, N, V, L);
    }
    // normal mapped
    else if (uTechnique == 5)
    {
        N = normalize(tbn * normalMap);
        fColor = shading(colorMap, N, V, L);
    }
    // parallax mapped
    else if (uTechnique == 6)
    {
        // same as normal mapping
        N = normalize(tbn * normalMap);
        fColor = shading(colorMap, N, V, L);
    }

    // show normals
    if (uShowNormals)
        fColor = N;
}
