layout(vertices = 4) out;

uniform float uTessellationLevel;

in vec2 vTexCoord[];
in vec3 vNormal[];
in vec3 vTangent[];
in vec3 vWorldPos[];

out vec2 eTexCoord[];
out vec3 eNormal[];
out vec3 eTangent[];
out vec3 eWorldPos[];

void main(void)
{
    gl_TessLevelOuter[0] =
    gl_TessLevelOuter[1] =
    gl_TessLevelOuter[2] =
    gl_TessLevelOuter[3] =
    gl_TessLevelInner[0] =
    gl_TessLevelInner[1] = uTessellationLevel;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    eTexCoord[gl_InvocationID] = vTexCoord[gl_InvocationID];
    eNormal[gl_InvocationID] = vNormal[gl_InvocationID];
    eTangent[gl_InvocationID] = vTangent[gl_InvocationID];
    eWorldPos[gl_InvocationID] = vWorldPos[gl_InvocationID];
}