#include "WeightedOITSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/pipeline/RenderScene.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include <AntTweakBar.h>

#include <GLFW/glfw3.h>

using namespace glow;
using namespace glow::camera;

void WeightedOITSample::init()
{
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mCube = geometry::Cube<>().generate();
    getPipelineScene()->atmoScatterIntensity = 0;

    TwAddVarRW(tweakbar(), "Levels", TW_TYPE_INT32, &mLevels, "min=1");
    TwAddVarRW(tweakbar(), "Alpha", TW_TYPE_FLOAT, &mAlpha, "min=0 step=0.1");
    TwAddVarRW(tweakbar(), "Start Size", TW_TYPE_FLOAT, &mStartSize, "min=0 step=0.1");
    TwAddVarRW(tweakbar(), "Size Increment", TW_TYPE_FLOAT, &mSizeInc, "min=0 step=0.1");
    TwAddVarRW(tweakbar(), "Hue Increment", TW_TYPE_FLOAT, &mHueInc, "min=0 step=0.1");
    TwAddVarRW(tweakbar(), "Stacked", TW_TYPE_BOOLCPP, &mStacked, "");
}

void WeightedOITSample::onRenderTransparentPass(glow::pipeline::RenderContext const& ctx)
{
    auto shader = ctx.useProgram(mShader);

    shader.setUniform("uRuntime", static_cast<GLfloat>(glfwGetTime()));
    shader.setUniform("uView", ctx.camData.view);
    shader.setUniform("uProj", ctx.camData.proj);

    shader.setUniform("uAlpha", mAlpha);

    for (auto i = 0; i < mLevels; ++i)
    {
        shader.setUniform("uColor", rgbColor(glm::vec3(mHueInc * i, 1.0f, 1.0f)));

        if (mStacked)
            shader.setUniform("uModel", glm::scale(glm::vec3(mStartSize + i * mSizeInc)));
        else
            shader.setUniform("uModel", glm::translate(glm::vec3(cos(i * 2 * glm::pi<double>() / mLevels), 0,
                                                                 sin(i * 2 * glm::pi<double>() / mLevels)))
                                            * glm::scale(glm::vec3(mStartSize)));

        mCube->bind().draw();
    }
}

WeightedOITSample::WeightedOITSample()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    setUsePipeline(true);
    setUsePipelineConfigGui(false);
}
