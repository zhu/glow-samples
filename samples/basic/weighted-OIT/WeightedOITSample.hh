#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class WeightedOITSample : public glow::glfw::GlfwApp
{
private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mCube;

    int mLevels = 12;
    float mStartSize = 0.2f;
    float mSizeInc = 0.15f;
    float mAlpha = 0.6f;
    float mHueInc = 30;

    bool mStacked = false;

protected:
    void init() override;
    void onRenderTransparentPass(glow::pipeline::RenderContext const& ctx) override;

public:
    WeightedOITSample();
};
