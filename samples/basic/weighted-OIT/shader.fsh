#glow pipeline transparent

#include <glow-pipeline/pass/transparent/oitPass.glsl>

uniform vec3 uColor;
uniform float uAlpha;

in vec3 vPosition;
in vec3 vNormal;

void main() {

    vec3 color = uColor * abs(dot(normalize(vNormal), normalize(vec3(1,3,0.4))));

    outputOitGeometry(gl_FragCoord.z, color * uAlpha, uAlpha);
}
