#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class PostProcessingSample : public glow::glfw::GlfwApp
{
public:
    PostProcessingSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glm::vec3 mLightColor = glm::vec3(1);
    glm::vec3 mLightDir = normalize(glm::vec3(-.2f, 1, .1f));
    float mLightDistance = 10.0f;

    bool mMouseLeft = false;

private:
    glow::SharedFramebuffer mFramebufferSolid;
    glow::SharedTextureRectangle mTexColor;
    glow::SharedTextureRectangle mTexNormal;
    glow::SharedTextureRectangle mTexPosition;
    glow::SharedTextureRectangle mTexDepth;
    glow::SharedTextureRectangle mTexVelocity;

    glow::SharedProgram mShaderObject;
    glow::SharedProgram mShaderBackground;

    glow::SharedVertexArray mMeshSuzanne;
    glow::SharedVertexArray mMeshPlane;
    glow::SharedVertexArray mMeshQuad;

    glow::SharedTextureCubeMap mTexSkybox;

private: // post-processing
    glow::SharedTextureRectangle mTexPostCurr;
    glow::SharedTextureRectangle mTexPostNext;

    glow::SharedFramebuffer mFramebufferToCurr;
    glow::SharedFramebuffer mFramebufferToNext;

    glow::SharedProgram mShaderCopy;

    glow::SharedProgram mShaderDithering;
    glow::SharedProgram mShaderGammaCorrection;
    glow::SharedProgram mShaderFXAA;
    glow::SharedProgram mShaderDownsample2;
    glow::SharedProgram mShaderSobel;
    glow::SharedProgram mShaderSharpen;
    glow::SharedProgram mShaderMotionBlur;
    glow::SharedProgram mShaderVignette;
    glow::SharedProgram mShaderGrain;
    glow::SharedProgram mShaderChromaticAberrations;
    glow::SharedProgram mShaderOutline;
    glow::SharedProgram mShaderQuantization;

    glow::SharedProgram mShaderZoom;

    bool mWireframe = false;
    bool mBackfaceCulling = true;

    bool mEnableDithering = true;
    bool mEnableFXAA = true;
    bool mEnableGammaCorrection = true;
    bool mEnableMotionBlur = false;
    bool mEnableVignette = false;
    bool mEnableGrain = false;
    bool mEnableChromaticAberrations = false;
    bool mEnableSimpleOutline = false;
    bool mEnableOutline = false;

    bool mEnableSobel = false;
    float mSharpen = 0.0f;

    glm::vec3 mOutlineColor = {1,0,0};

    int mQuantization = 0;

    float mSimpleOutlineStrength = 0.01f;

    float mVignetteA = 0.75;
    float mVignetteB = 0.45;

    float mOutlineNormal = 1.0f;
    float mOutlineDepth = 1.0f;

    float mAberrationStrength = 10;

    float mGrainSize = 5;
    float mGrainStrength = 0.2;
    float mGrainSpeed = 0.4;

    float mZoomLvl = 1;
    glm::vec2 mZoomPos;

    int mDownsampling = 0;

    double mRuntime = 0.0;
    double mPrevRuntime = 0.0;
    float mSpeed = 1.0;
    bool mAnimate = true;

    float mElapsedSeconds;

    float mMotionBlurLength = 30.0f;
    
    glm::mat4 mPrevView;
    glm::mat4 mPrevProj;

    void copyTo(glow::SharedTextureRectangle const& from, glow::SharedFramebuffer const& to) const;

    void apply(glow::SharedProgram const& program, int w = -1, int h = -1);
    void ppSwap();

protected:
    void init() override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;

    bool onMousePosition(double x, double y) override;
    bool onMouseButton(double x, double y, int button, int action, int mods, int clickCount) override;
};
