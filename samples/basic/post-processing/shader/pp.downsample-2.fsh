uniform sampler2DRect uTexture;

out vec4 fColor;

void main() 
{
    vec4 color = texture(uTexture, gl_FragCoord.xy * 2);
    fColor = color;
}
