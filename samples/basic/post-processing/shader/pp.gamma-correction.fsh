uniform sampler2DRect uTexture;

out vec4 fColor;

void main() 
{
    vec4 color = texelFetch(uTexture, ivec2(gl_FragCoord.xy));
    color.rgb = pow(color.rgb, vec3(1 / 2.224));
    fColor = color;
}
