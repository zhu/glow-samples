uniform sampler2DRect uTexture;
uniform float uSharpen;

out vec4 fColor;

void main() 
{
    vec4 c = texelFetch(uTexture, ivec2(gl_FragCoord.xy));

    vec4 c0 = texture(uTexture, gl_FragCoord.xy + vec2(-0.5, -0.5));
    vec4 c1 = texture(uTexture, gl_FragCoord.xy + vec2(-0.5, +0.5));
    vec4 c2 = texture(uTexture, gl_FragCoord.xy + vec2(+0.5, -0.5));
    vec4 c3 = texture(uTexture, gl_FragCoord.xy + vec2(+0.5, +0.5));

    vec4 lapl = 4 * c - c0 - c1 - c2 - c3;

    fColor = c + lapl * uSharpen;
}
