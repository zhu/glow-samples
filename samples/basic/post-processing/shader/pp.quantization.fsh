uniform sampler2DRect uTexture;

uniform float uQuantization;

out vec4 fColor;

void main() 
{
    vec4 color = texelFetch(uTexture, ivec2(gl_FragCoord.xy));
    fColor = round(color * 256 / uQuantization) / 256 * uQuantization;
}
