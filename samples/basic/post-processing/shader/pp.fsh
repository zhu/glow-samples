uniform sampler2DRect uTexture;

out vec4 fColor;

void main() 
{
    vec4 color = texelFetch(uTexture, ivec2(gl_FragCoord.xy));
    fColor = color;
}
