uniform vec3 uColor;
uniform vec3 uLightPos;
uniform vec3 uLightColor;
uniform float uElapsedSeconds;

uniform bool uOutline;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vPrevPosition;

out vec3 fColor;
out vec3 fNormal;
out vec3 fPosition;
out vec3 fVelocity;

void main() 
{
    vec3 N = normalize(vNormal);
    vec3 L = normalize(uLightPos - vPosition);

    fColor = pow(uColor, vec3(2.224)) * (0.5 + 0.5 * dot(normalize(vNormal), L)) * pow(uLightColor, vec3(2.224));
    if (uOutline)
        fColor = uColor;

    fNormal = N;
    fPosition = vPosition;
    fVelocity = (vPosition - vPrevPosition) / uElapsedSeconds;
}
