uniform sampler2DRect uTexture;

out vec4 fColor;

void main()
{
    // see https://github.com/Jam3/glsl-fast-gaussian-blur/blob/master/9.glsl

    vec2 uv = gl_FragCoord.xy;
    vec2 direction = vec2(0, 1);
    vec2 off1 = vec2(1.3846153846) * direction;
    vec2 off2 = vec2(3.2307692308) * direction;

    fColor = vec4(0);
    fColor += texture(uTexture, uv) * 0.2270270270;
    fColor += texture(uTexture, uv + off1) * 0.3162162162;
    fColor += texture(uTexture, uv - off1) * 0.3162162162;
    fColor += texture(uTexture, uv + off2) * 0.0702702703;
    fColor += texture(uTexture, uv - off2) * 0.0702702703;
}