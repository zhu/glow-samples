layout(vertices = 4) out;

uniform float uWidth;
uniform float uHeight;
uniform float uEdgeLength;
uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uViewProj;

in vec3 vWorldPos[];

float tessLvl(int ia, int ib)
{
    vec4 a = uView * gl_in[ia].gl_Position;
    vec4 b = uView * gl_in[ib].gl_Position;
    float dis = distance(a, b);
    float mz = (a.z + b.z) / 2;

    if (mz > -0.5) mz = -0.5;

    vec4 ssa = uProj * vec4(0.0, 0.0, mz, 1.0);
    vec4 ssb = uProj * vec4(dis, dis, mz, 1.0);

    vec2 sa = ssa.xy / ssa.w;
    vec2 sb = ssb.xy / ssb.w;
    sa = (sa * 0.5 + 0.5) * vec2(uWidth, uHeight);
    sb = (sb * 0.5 + 0.5) * vec2(uWidth, uHeight);

    float el = distance(sa, sb);

    return clamp(el / uEdgeLength, 1.0, 64.0);

    /*
    vec4 a = uView * gl_in[ia].gl_Position;
    vec4 b = uView * gl_in[ib].gl_Position;
    float 
    vec2 sa = a.xy / a.w;
    vec2 sb = b.xy / b.w;
    sa = (sa * 0.5 + 0.5) * vec2(uWidth, uHeight);
    sb = (sb * 0.5 + 0.5) * vec2(uWidth, uHeight);

    float el = distance(sa, sb);

    return clamp(el / uEdgeLength, 1.0, 64.0);
    */
}

void main(void)
{
    // TODO: calc level
    float lvl = 1.0;

    gl_TessLevelOuter[0] = tessLvl(1, 2);
    gl_TessLevelOuter[1] = tessLvl(0, 1);
    gl_TessLevelOuter[2] = tessLvl(3, 0);
    gl_TessLevelOuter[3] = tessLvl(2, 3);

    gl_TessLevelInner[0] = (gl_TessLevelOuter[1] + gl_TessLevelOuter[3]) / 2.0;
    gl_TessLevelInner[1] = (gl_TessLevelOuter[0] + gl_TessLevelOuter[2]) / 2.0;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}