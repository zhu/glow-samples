#include "terrain.glsl"

layout(quads, fractional_even_spacing, ccw) in;

uniform mat4 uViewProj;
uniform float uQuadSize;

out vec3 tWorldPos;
out float tWorldEdge;

vec4 bmix4(vec4 p0, vec4 p1, vec4 p2, vec4 p3)
{
    vec4 p01 = mix(p1, p0, gl_TessCoord.x);
    vec4 p23 = mix(p2, p3, gl_TessCoord.x);

    return mix(p01, p23, gl_TessCoord.y);
}
vec3 bmix3(vec3 p0, vec3 p1, vec3 p2, vec3 p3)
{
    vec3 p01 = mix(p1, p0, gl_TessCoord.x);
    vec3 p23 = mix(p2, p3, gl_TessCoord.x);

    return mix(p01, p23, gl_TessCoord.y);
}
vec2 bmix2(vec2 p0, vec2 p1, vec2 p2, vec2 p3)
{
    vec2 p01 = mix(p1, p0, gl_TessCoord.x);
    vec2 p23 = mix(p2, p3, gl_TessCoord.x);

    return mix(p01, p23, gl_TessCoord.y);
}
float bmix1(float p0, float p1, float p2, float p3)
{
    float p01 = mix(p1, p0, gl_TessCoord.x);
    float p23 = mix(p2, p3, gl_TessCoord.x);

    return mix(p01, p23, gl_TessCoord.y);
}

void main()
{
    vec4 worldPos = bmix4(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[2].gl_Position, gl_in[3].gl_Position);

    float tess0 = (gl_TessLevelOuter[1] + gl_TessLevelOuter[2]) / 2;
    float tess1 = (gl_TessLevelOuter[0] + gl_TessLevelOuter[1]) / 2;
    float tess2 = (gl_TessLevelOuter[0] + gl_TessLevelOuter[3]) / 2;
    float tess3 = (gl_TessLevelOuter[3] + gl_TessLevelOuter[2]) / 2;
    float tess = bmix1(tess0, tess1, tess2, tess3);
    tWorldEdge = uQuadSize / tess;

    worldPos.y = terrainAt(worldPos.x, worldPos.z, uQuadSize / 64.0);

    tWorldPos = worldPos.xyz;
    gl_Position = uViewProj * worldPos;
}