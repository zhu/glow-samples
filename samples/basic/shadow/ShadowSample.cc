#include "ShadowSample.hh"

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>

#include <AntTweakBar.h>

#include <glm/ext.hpp>

using namespace glow;
using namespace glow::camera;


namespace
{
struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
};
}


void ShadowSample::createShadowFrameBuffer()
{
    mTextureShadow = TextureRectangle::create(mShadowSize, mShadowSize, GL_DEPTH_COMPONENT32);
    mTextureShadow->bind().setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);

    // No color attachements, only depth attachment
    mFrameBufferShadow = Framebuffer::createDepthOnly(mTextureShadow);
}

void ShadowSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!


    // Create the shaders
    std::string path = util::pathOf(__FILE__);
    mShaderSuzanne = Program::createFromFile(path + "/object");
    mShaderPlane = Program::createFromFiles({path + "/object.vsh", path + "/plane.fsh"});
    mShaderShadow = Program::createFromFiles({path + "/object.vsh", path + "/shadow.fsh"});

    // Upload vertex data for ground plane
    Vertex vertices[4] = {
        {glm::vec3(-2, -1, -2), glm::vec3(0, 1, 0)},
        {glm::vec3(-2, -1, +2), glm::vec3(0, 1, 0)},
        {glm::vec3(+2, -1, -2), glm::vec3(0, 1, 0)},
        {glm::vec3(+2, -1, +2), glm::vec3(0, 1, 0)},
    };
    uint16_t indices[3 * 2] = {0, 2, 1, 1, 2, 3};

    SharedArrayBuffer abPlane = ArrayBuffer::create();
    abPlane->defineAttribute(&Vertex::position, "aPosition");
    abPlane->defineAttribute(&Vertex::normal, "aNormal");
    abPlane->bind().setData(vertices);
    SharedElementArrayBuffer eabPlane = ElementArrayBuffer::create(indices);
    mPlane = VertexArray::create(abPlane, eabPlane, GL_TRIANGLES);

    // Import geometry
    mSuzanne = assimp::Importer().load(util::pathOf(__FILE__) + "/../../../data/suzanne.obj");

    createShadowFrameBuffer();

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});

    // Create new items in AntTweakBar
    TwAddVarRW(tweakbar(), "Rotate", TW_TYPE_BOOLCPP, &mAnimate, "group=rendering");
    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightPos, "group=shadow");
    TwAddVarRW(tweakbar(), "Self Shadowing", TW_TYPE_BOOLCPP, &shadowSelf, "group=shadow");
    TwAddVarRW(tweakbar(), "Shadow Offset", TW_TYPE_BOOLCPP, &shadowOffset, "group=shadow");

    const char* twOptions = "group=shadow, enum='16 {16}, 64 {64}, 256 {256}, 512 {512}, 1024 {1024}, 2048 {2048}, 4096 {4096}'";
    TwAddVarRW(tweakbar(), "Shadow Resolution", TwDefineEnum("", nullptr, 0), &mShadowSize, twOptions);
}


void ShadowSample::render(float elapsedSeconds)
{
    if(mLastShadowSize != mShadowSize)
    {
        // Recreate shadow map and framebuffer
        createShadowFrameBuffer();
        mLastShadowSize = mShadowSize;
    }

    // Functions called with this macro are undone at the end of the scope
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(depthFunc, GL_LESS);

    if (mAnimate)
        mRuntime += elapsedSeconds;

    // Create model matrix for rotation
    glm::mat4 modelMatrix = glm::rotate(float(mRuntime), glm::vec3(0, 1, 0));

    // Get the projection and view matrices from the GlowApp-Camera
    glm::mat4 proj = getCamera()->getProjectionMatrix();
    glm::mat4 view = getCamera()->getViewMatrix();

    // Compute shadow matrices
    glm::mat4 shadowProjMatrix = glm::perspective(glm::pi<float>() / 5.0f, 1.0f, 1.0f, 100.0f);
    glm::mat4 shadowViewMatrix = glm::lookAt(mLightPos, glm::vec3(0.0f), glm::vec3(0, 1, 0));
    glm::mat4 shadowViewProjMatrix = shadowProjMatrix * shadowViewMatrix;


    // Pass 1: Render the object depth (as seen from the light) into the shadow texture
    {
        // Create a BoundFrameBuffer that is automatically unbound at the end of the scope
        auto fb = mFrameBufferShadow->bind();

        // Clear the depth texture (shadow texture) attached to this frame buffer
        glClear(GL_DEPTH_BUFFER_BIT);

        auto shader = mShaderShadow->use();
        shader.setUniform("uModelMatrix", modelMatrix);
        shader.setUniform("uViewMatrix", shadowViewMatrix);
        shader.setUniform("uProjectionMatrix", shadowProjMatrix);
        shader.setUniform("uLightPos", mLightPos);

        mSuzanne->bind().draw();
    }

    // Clear background to bluish color and restore previous color at the end of the scope
    GLOW_SCOPED(clearColor, 0.0, 0.33, 0.62, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Pass 2: Render ground plane and objects
    {
        auto shader = mShaderPlane->use();
        shader.setUniform("uModelMatrix", glm::translate(glm::vec3(0, -0.5, 0)) * glm::scale(glm::vec3(10, 1, 10)));
        shader.setUniform("uViewMatrix", view);
        shader.setUniform("uProjectionMatrix", proj);
        shader.setUniform("uShadowViewProjMatrix", shadowViewProjMatrix);
        shader.setUniform("uLightPos", mLightPos);
        shader.setUniform("uTexShadowSize", (float)mShadowSize);
        shader.setTexture("uTexShadow", mTextureShadow);

        mPlane->bind().draw();
    }
    {
        auto shader = mShaderSuzanne->use();
        shader.setUniform("uModelMatrix", modelMatrix);
        shader.setUniform("uViewMatrix", view);
        shader.setUniform("uProjectionMatrix", proj);
        shader.setUniform("uShadowViewProjMatrix", shadowViewProjMatrix);
        shader.setUniform("uLightPos", mLightPos);
        shader.setUniform("uTexShadowSize", (float)mShadowSize);
        shader.setUniform("uShadowOffset", shadowSelf ? shadowOffset ? 0.001f : 0.0f : 1.0f);
        shader.setTexture("uTexShadow", mTextureShadow);

        mSuzanne->bind().draw();
    }
}
