#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class CubeSample : public glow::glfw::GlfwApp
{
public:
    CubeSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mCube;

    float mRuntime = 0.0f;
    bool mAnimate = true;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
