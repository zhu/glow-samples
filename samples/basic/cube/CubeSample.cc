#include "CubeSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void CubeSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mCube = geometry::Cube<>().generate();

    auto cam = getCamera();
    cam->setLookAt({2, 5, 4}, {0, 0, 0});

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");
}

void CubeSample::render(float elapsedSeconds)
{
    mRuntime += elapsedSeconds;
    auto time = mAnimate ? mRuntime : 0.0f;

    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", time);
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uModel", glm::rotate(time, glm::vec3{0, 1, 0}));

    mCube->bind().draw();
}
