#include "uniforms.glsl"

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

// input _arrays_ (size 3)
in vec3 gPosition[3];
in float gLength[3];

// output to fragment shader
out vec3 vPosition;
out float vLength;
out vec3 vNormal;

void main() {
    // per-triangle calculation
    vec3 v0 = (gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 v1 = (gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 n = normalize(cross(v0, v1));

    // vertices
    for (int i = 0; i < 3; ++i) {
        vLength = gLength[i];
        vPosition = gPosition[i];
        vNormal = n;
        gl_Position = uProj * uView * gl_in[i].gl_Position;
        EmitVertex();
    }

    // emit triangle
    EndPrimitive();
}
