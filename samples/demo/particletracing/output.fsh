out vec4 fColor;

uniform sampler2DRect uTexture;

void main()
{
    fColor.rgb = texture(uTexture, gl_FragCoord.xy).rgb;
    vec3 v = clamp(fColor.rgb, vec3(0), vec3(1));

    //v = 0.5 + pow(abs(v - 0.5)*2, vec3(1.0)) * sign(v - 0.5) / 2;

    fColor.rgb = v;
    fColor.a = 1;
}