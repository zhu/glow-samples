in vec2 aPosition;
in vec2 aTexcoord;

uniform float uTime;
out vec2 vTexcoord;

void main()
{
    vTexcoord = aTexcoord;
    //vTexcoord.y += uTime;
    gl_Position = vec4(2 * aPosition - 1, 0, 1);
}
