#include "ParticleTracingSample.hh"

#include <algorithm>
#include <fstream>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/TimerQuery.hh>
#include <glow/objects/TransformFeedback.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/log.hh>
#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>

#include <AntTweakBar.h>

#include <GLFW/glfw3.h>

using namespace glow;

static TwEnumVal flowEV[] = {
    {ParticleTracingSample::Circular, "Circular"},     //
    {ParticleTracingSample::Parallel, "Parallel"},     //
    {ParticleTracingSample::Converging, "Converging"}, //
    {ParticleTracingSample::Diverging, "Diverging"},   //
    {ParticleTracingSample::Wave, "Wave"},             //
    {ParticleTracingSample::Vortices, "Vortices"}      //
};

static TwEnumVal methodEV[] = {{ParticleTracingSample::Euler, "Euler"},       //
                               {ParticleTracingSample::Midpoint, "Midpoint"}, //
                               {ParticleTracingSample::RK4, "Runge-Kutta"}};

// Creates 4 vortices in the corners and one "inverse vortex" in the center
static glm::vec2 vorticesFlow(glm::vec2 pos)
{
    glm::vec2 newPos = 2 * pos - glm::vec2(int(2 * pos.x) % 2, int(2 * pos.y) % 2);

    // Vector away from the center
    float dx = newPos.x - 0.5;
    float dy = newPos.y - 0.5;

    // Vector towards the center
    float dxx = 0.5 - pos.x;
    float dyy = 0.5 - pos.y;

    float newX = dy - 0.3 * dx;
    float newY = -dx - 0.3 * dy;

    float a = 4 * (dxx * dxx + dyy * dyy);

    return glm::mix(glm::vec2(dyy - 0.3 * dxx, -dxx - 0.3 * dyy), glm::vec2(newX, newY), a);
}

static glm::vec2 circularFlow(glm::vec2 pos)
{
    float dx = 0.5 - pos.x;
    float dy = 0.5 - pos.y;
    float dis = sqrt(dx * dx + dy * dy);
    dx /= dis;
    dy /= dis;
    return glm::vec2(dy, -dx);
}

static glm::vec2 parallelFlow(glm::vec2 pos)
{
    return glm::vec2(1, 0);
}

static glm::vec2 convergingFlow(glm::vec2 pos)
{
    float dx = 0.5 - pos.x;
    float dy = 0.5 - pos.y;
    float dis = sqrt(dx * dx + dy * dy);
    dx /= dis;
    dy /= dis;
    return glm::vec2(dx, dy);
}

static glm::vec2 divergingFlow(glm::vec2 pos)
{
    float dx = 0.5 - pos.x;
    float dy = 0.5 - pos.y;
    float dis = sqrt(dx * dx + dy * dy);
    dx /= dis;
    dy /= dis;
    return glm::vec2(-dx, -dy);
}

static glm::vec2 waveFlow(glm::vec2 pos)
{
    return glm::vec2(1, std::sin(4 * glm::pi<float>() * pos.x));
}

static glm::vec2 generateFlow(glm::vec2 pos, ParticleTracingSample::FlowType ftype)
{
    switch (ftype)
    {
    case ParticleTracingSample::Circular:
        return circularFlow(pos);

    case ParticleTracingSample::Parallel:
        return parallelFlow(pos);

    case ParticleTracingSample::Converging:
        return convergingFlow(pos);

    case ParticleTracingSample::Diverging:
        return divergingFlow(pos);

    case ParticleTracingSample::Wave:
        return waveFlow(pos);

    case ParticleTracingSample::Vortices:
        return vorticesFlow(pos);

    default:
        return {0, 0};
    }
}

static glm::vec2 linelineIntersection(glm::vec2 a1, glm::vec2 a2, glm::vec2 b1, glm::vec2 b2, bool& okay)
{
    okay = true;

    auto lengthA = glm::length(a1 - a2);
    auto lengthB = glm::length(b1 - b2);
    if (lengthA < 1e-9 || lengthB < 1e-9)
    {
        okay = false;
        return glm::vec2(0, 0);
    }

    auto dir1 = (a1 - a2) / lengthA;
    auto dir2 = (b1 - b2) / lengthB;
    if (std::abs(glm::dot(dir1, dir2)) > 0.95)
    {
        // Almost parallel, avoid instability
        okay = false;
        return glm::vec2(0, 0);
    }

    const double eps = 1e-12;
    double denom = (a1[0] - a2[0]) * (b1[1] - b2[1]) - (a1[1] - a2[1]) * (b1[0] - b2[0]);
    if (fabs(denom) < eps)
    {
        okay = false;
        return glm::vec2(0, 0);
    }

    auto res = glm::vec2((a1[0] * a2[1] - (a1[1] * a2[0])) * (b1[0] - b2[0]) - (b1[0] * b2[1] - (b1[1] * b2[0])) * (a1[0] - a2[0]),
                         (a1[0] * a2[1] - (a1[1] * a2[0])) * (b1[1] - b2[1]) - (b1[0] * b2[1] - (b1[1] * b2[0])) * (a1[1] - a2[1]));

    res *= glm::vec2(1.0 / denom);
    return res;
}

void ParticleTracingSample::generateFlowField()
{
    mVelocityField.clear();
    mVelocityField.reserve(mResX * mResY);

    float sx = 1.0 / mResX;
    float sy = 1.0 / mResY;

    for (int y = 0; y < mResY; ++y)
        for (int x = 0; x < mResX; ++x)
            mVelocityField.push_back(generateFlow(glm::vec2(sx * (x + 0.5), sy * (y + 0.5)), mFlowType));
}

void ParticleTracingSample::generateBuffers()
{
    auto w = mResX;
    auto h = mResY;

    struct Vertex
    {
        glm::vec2 pos;
        glm::vec2 uv;
    };

    std::vector<Vertex> arrowBuffer;
    arrowBuffer.reserve(w * h * 6); // 2 triangles per arrow

    glm::vec2 spacing = glm::vec2(1.0 / (w), 1.0 / (h));
    glm::vec2 size = 0.01 * mSize * spacing;

    int c = 0;
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
        {
            glm::vec2 dirUp = mVelocityField[c++];
            float speed = glm::length(dirUp);
            if (speed > 0)
                dirUp /= speed;
            else
            {
                dirUp = glm::vec2(0, 1);
                speed = 1.0f;
            }
            glm::vec2 dirRight = {dirUp.y, -dirUp.x};
            glm::vec2 pos = {(x + 0.5) * spacing.x, (y + 0.5) * spacing.y};

            glm::vec2 thisSize = size;
            // thisSize.y *= speed;
            // thisSize.x *= speed;

            arrowBuffer.push_back({{pos + 0.5 * thisSize.y * dirUp + 0.5 * thisSize.x * dirRight}, {1, 0}}); // top right
            arrowBuffer.push_back({{pos + 0.5 * thisSize.y * dirUp - 0.5 * thisSize.x * dirRight}, {0, 0}}); // top left
            arrowBuffer.push_back({{pos - 0.5 * thisSize.y * dirUp - 0.5 * thisSize.x * dirRight}, {0, 1}}); // bottom left

            arrowBuffer.push_back({{pos + 0.5 * thisSize.y * dirUp + 0.5 * thisSize.x * dirRight}, {1, 0}}); // top right
            arrowBuffer.push_back({{pos - 0.5 * thisSize.y * dirUp - 0.5 * thisSize.x * dirRight}, {0, 1}}); // bottom left
            arrowBuffer.push_back({{pos - 0.5 * thisSize.y * dirUp + 0.5 * thisSize.x * dirRight}, {1, 1}}); // bottom right
        }

    auto ab = ArrayBuffer::create();
    ab->defineAttribute(&Vertex::pos, "aPosition");
    ab->defineAttribute(&Vertex::uv, "aTexcoord");
    ab->bind().setData(arrowBuffer);
    mArrowField = VertexArray::create(ab);


    std::vector<Vertex> particlePointBuffer;

    for (auto const& tail : mParticlePositions)
    {
        for (auto i = 0u; i < tail.size(); ++i)
        {
            particlePointBuffer.push_back({{tail[i]}, {0.5, 0.5}});
        }
    }
    {
        auto ab = ArrayBuffer::create();
        ab->defineAttribute(&Vertex::pos, "aPosition");
        ab->defineAttribute(&Vertex::uv, "aTexcoord");
        ab->bind().setData(particlePointBuffer);
        mParticlePoints = VertexArray::create(ab, GL_POINTS);
    }


    // TODO: triangle strip?
    std::vector<Vertex> particleBuffer;
    particleBuffer.reserve(6 * mSteps * mParticlePositions.size()); // 2 triangles per quad, 1 quad per position-pair

    for (auto const& tail : mParticlePositions)
    {
        // std::cout << "Tail: " << tail.size() << std::endl;
        if (tail.size() < 2u)
            continue;

        glm::vec2 lastLeft, lastRight;
        for (auto i = 1u; i < tail.size(); ++i)
        {
            glm::vec2 p1 = tail[i];
            glm::vec2 p0 = tail[i - 1u];

            glm::vec2 dir = p1 - p0;
            float dist = glm::length(dir);
            if (dist < 1e-15)
            {
                dist = 1e-15;
            }

            dir /= dist;
            glm::vec2 right = {dir.y, -dir.x};

            glm::vec2 bottomLeft = p0 - mTailWidth * right;
            glm::vec2 bottomRight = p0 + mTailWidth * right;
            glm::vec2 topLeft = p1 - mTailWidth * right;
            glm::vec2 topRight = p1 + mTailWidth * right;

            // For all but the first tail segment
            if (i > 1)
            {
                bottomLeft = lastLeft;
                bottomRight = lastRight;
            }

            // For all but the last element...
            if (i + 1 < tail.size())
            {
                glm::vec2 p2 = tail[i + 1u];

                glm::vec2 nextDir = glm::normalize(p2 - p1);
                glm::vec2 nextRight = {nextDir.y, -nextDir.x};
                glm::vec2 nextBottomLeft = p1 - mTailWidth * nextRight;
                glm::vec2 nextBottomRight = p1 + mTailWidth * nextRight;
                glm::vec2 nextTopLeft = p2 - mTailWidth * nextRight;
                glm::vec2 nextTopRight = p2 + mTailWidth * nextRight;

                bool okay;
                auto newLeft = linelineIntersection(bottomLeft, topLeft, nextBottomLeft, nextTopLeft, okay);
                if (okay)
                    topLeft = newLeft;
                auto newRight = linelineIntersection(bottomRight, topRight, nextBottomRight, nextTopRight, okay);
                if (okay)
                    topRight = newRight;

                lastLeft = topLeft;
                lastRight = topRight;
            }

            // glow::info() << bottomLeft << " " << bottomRight << " " << topLeft << " " << topRight;

            particleBuffer.push_back({bottomLeft, {0, 1}});  // bottom left
            particleBuffer.push_back({bottomRight, {1, 1}}); // bottom right
            particleBuffer.push_back({topLeft, {0, 0}});     // top left

            particleBuffer.push_back({topLeft, {0, 0}});     // top left
            particleBuffer.push_back({bottomRight, {1, 1}}); // bottom right
            particleBuffer.push_back({topRight, {1, 0}});    // top right
        }
    }

    {
        auto ab = ArrayBuffer::create();
        ab->defineAttribute(&Vertex::pos, "aPosition");
        ab->defineAttribute(&Vertex::uv, "aTexcoord");
        ab->bind().setData(particleBuffer);
        mParticleTails = VertexArray::create(ab);
    }
}

glm::vec2 ParticleTracingSample::getVelocity(glm::vec2 pos)
{
    pos = glm::min(glm::max(pos, glm::vec2(0, 0)), glm::vec2(1, 1));
    glm::vec2 scaledPos = pos * glm::vec2(mResX, mResY) - glm::vec2(0.5);

    // Bilinear interpolation
    int sampleX = std::min(mResX - 1, int(scaledPos.x));
    int sampleY = std::min(mResY - 1, int(scaledPos.y));

    int sampleX1 = std::min(mResX - 1, int(scaledPos.x + 1));
    int sampleY1 = std::min(mResY - 1, int(scaledPos.y + 1));

    float u = scaledPos.x - sampleX;
    float v = scaledPos.y - sampleY;

    glm::vec2 topVel = glm::mix(mVelocityField[sampleX + mResX * sampleY], mVelocityField[sampleX1 + mResX * sampleY], u);
    glm::vec2 bottomVel = glm::mix(mVelocityField[sampleX + mResX * sampleY1], mVelocityField[sampleX1 + mResX * sampleY1], u);
    glm::vec2 vel = glm::mix(topVel, bottomVel, v);

    return vel;
}

glm::vec2 ParticleTracingSample::integrate(glm::vec2 pos, float dt)
{
    switch (mMethod)
    {
    case Euler:
    {
        return pos + dt * getVelocity(pos);
    }

    case Midpoint:
    {
        auto midp = pos + dt * getVelocity(pos) / 2.0f;
        return pos + dt * getVelocity(midp);
    }

    case RK4:
    {
        auto k1 = getVelocity(pos);
        auto k2 = getVelocity(pos + dt * k1 / 2);
        auto k3 = getVelocity(pos + dt * k2 / 2);
        auto k4 = getVelocity(pos + dt * k3);
        return pos + dt * (k1 + 2 * k2 + 2 * k3 + k4) / 6.0f;
    }

    default:
        glow::error() << "Unsupported method!";
        return pos;
    }
}

std::vector<glm::vec2> ParticleTracingSample::simulateOneParticle(glm::vec2 pos, float dt)
{
    std::vector<glm::vec2> tail = {pos};

    auto currPos = pos;
    for (int i = 0; i < mSteps; ++i)
    {
        currPos = integrate(currPos, dt);
        tail.push_back(currPos);
    }

    return tail;
}


void ParticleTracingSample::simulateParticles()
{
    mParticlePositions.clear();

    for (auto sp : mStartPositions)
    {
        auto tail = simulateOneParticle(sp, mDT);
        mParticlePositions.push_back(tail);
    }
}

void ParticleTracingSample::init()
{
    setUsePipeline(true);
    setUseDefaultCameraHandling(false);

    setGui(GlfwApp::Gui::AntTweakBar);

    // IMPORTANT: call base function
    glfw::GlfwApp::init();

    auto basePath = util::pathOf(__FILE__);
    mShader = Program::createFromFile(basePath + "/arrows");

    mArrow = Texture2D::createFromFile(util::pathOf(__FILE__) + "/arrow.png", ColorSpace::sRGB);
    mParticle = Texture2D::createFromFile(util::pathOf(__FILE__) + "/particle.png", ColorSpace::sRGB);

    TwAddVarRW(tweakbar(), "Time", TW_TYPE_FLOAT, &mTime, "step=0.002");
    TwAddVarRW(tweakbar(), "dt", TW_TYPE_FLOAT, &mDT, "step=0.01");
    TwAddVarRW(tweakbar(), "Steps", TW_TYPE_INT32, &mSteps, "min = 1");
    TwAddVarRW(tweakbar(), "Res X", TW_TYPE_INT32, &mResX, "min = 1, max = 4096");
    TwAddVarRW(tweakbar(), "Res Y", TW_TYPE_INT32, &mResY, "min = 1, max = 4096");
    TwAddVarRW(tweakbar(), "Arrow Size [%]", TW_TYPE_INT32, &mSize, "min = 0, max = 200");
    TwAddVarRW(tweakbar(), "Particle Tail Width", TW_TYPE_FLOAT, &mTailWidth, "min = 0.001, max = 0.1, step =0.001");
    TwAddVarRW(tweakbar(), "Flow Type", TwDefineEnum("FlowType", flowEV, ParticleTracingSample::FlowTypeCount), &mFlowType, "");
    TwAddVarRW(tweakbar(), "Method", TwDefineEnum("Method", methodEV, ParticleTracingSample::MethodCount), &mMethod, "");

    generateFlowField();
    simulateParticles();
    generateBuffers();

    glPointSize(50);
}

void ParticleTracingSample::update(float elapsedSeconds)
{
    // IMPORTANT: call base function
    glfw::GlfwApp::update(elapsedSeconds);

    mTime += elapsedSeconds;
}

void ParticleTracingSample::onResize(int w, int h)
{
    // IMPORTANT: call base function
    glfw::GlfwApp::onResize(w, h);
}

void ParticleTracingSample::onRenderOpaquePass(glow::pipeline::RenderContext const& ctx)
{
    glm::vec3 tailColor = glm::pow(glm::vec3(0 / 255.f, 84 / 255.f, 159 / 255.f), glm::vec3(2.2f));

    GLOW_SCOPED(disable, GL_CULL_FACE);
    GLOW_SCOPED(disable, GL_DEPTH_TEST);

    {
        generateFlowField();
        simulateParticles();
        generateBuffers();
    }

    auto shader = ctx.useProgram(mShader);

    // Set uniforms
    shader.setUniform("uWidth", getWindowWidth());
    shader.setUniform("uHeight", getWindowHeight());
    shader.setUniform("uTime", mTime);

    // Draw arrows
    shader.setTexture("uTexture", mArrow);
    shader.setUniform("uColor", glm::vec4(0.3, 0.3, 0.3, 1.0));
    mArrowField->bind().draw();

    // Draw Particle tail
    shader.setTexture("uTexture", mParticle);
    shader.setUniform("uColor", glm::vec4(tailColor, 1.0));
    mParticleTails->bind().draw();

    /*
    // Draw Particle Point
    shader.setTexture("uTexture", mParticle);
    shader.setUniform("uColor", glm::vec4(0.9, 0.1, 0.0, 1.0));
    mParticlePoints->bind().draw();
    */
}

bool ParticleTracingSample::onMousePosition(double x, double y)
{
    if (GlfwApp::onMousePosition(x, y))
        return true;

    if (isMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
    {
        mStartPositions.clear();
        mStartPositions.push_back({x / getWindowWidth(), //
                                   (getWindowHeight() - y) / getWindowHeight()});

        return true;
    }


    return false;
}

bool ParticleTracingSample::onMouseButton(double x, double y, int button, int action, int mods, int clickCount)
{
    if (GlfwApp::onMouseButton(x, y, button, action, mods, clickCount))
        return true;

    // Note: We to do this on button press as well, so that we do not have to always move the mouse
    if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
    {
        mStartPositions.clear();
        mStartPositions.push_back({x / getWindowWidth(), //
                                   (getWindowHeight() - y) / getWindowHeight()});

        return true;
    }

    return false;
}

bool ParticleTracingSample::onKey(int key, int scancode, int action, int mods)
{
    glfw::GlfwApp::onKey(key, scancode, action, mods);

    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
        mStartPositions.clear();

        simulateParticles();
        generateBuffers();

        return true;
    }

    return false;
}
