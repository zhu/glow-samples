#include "FlowAnimSample.hh"

#include <algorithm>
#include <fstream>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/AtomicCounterBuffer.hh>
#include <glow/objects/OcclusionQuery.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/PrimitiveQuery.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/Texture3D.hh>
#include <glow/objects/TimerQuery.hh>
#include <glow/objects/TransformFeedback.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/debugging/DebugRenderer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>

#include <AntTweakBar.h>

#include <GLFW/glfw3.h>

using namespace glow;

namespace
{
typedef struct
{
    uint64_t state;
    uint64_t inc;
} pcg32_random_t;

uint32_t pcg32_random_r(pcg32_random_t* rng)
{
    uint64_t oldstate = rng->state;
    // Advance internal state
    rng->state = oldstate * 6364136223846793005ULL + (rng->inc | 1);
    // Calculate output function (XSH RR), uses old state for max ILP
    uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
    uint32_t rot = oldstate >> 59u;
    return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}
}

void FlowAnimSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    glfw::GlfwApp::init();

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/quad");
    mShaderOutput
        = Program::createFromFiles({util::pathOf(__FILE__) + "/quad.vsh", util::pathOf(__FILE__) + "/output.fsh"});
    mQuad = geometry::Quad<>().generate();

    TwAddVarRW(tweakbar(), "Time", TW_TYPE_FLOAT, &mTime, "step=0.002");
    TwAddVarRW(tweakbar(), "Anim", TW_TYPE_FLOAT, &mAnim, "step=0.002");
    TwAddVarRW(tweakbar(), "Steps", TW_TYPE_INT32, &mSteps, "");

    mResultA = TextureRectangle::create(1, 1, GL_RGBA32F);
    mResultB = TextureRectangle::create(1, 1, GL_RGBA32F);
    mFBOA = Framebuffer::create({{"fColor", mResultA}});
    mFBOB = Framebuffer::create({{"fColor", mResultB}});

    mNoise = Texture3D::create(128, 128, 128, GL_RGBA32F);
    pcg32_random_t s = {17, 17};
    std::vector<glm::vec4> data;
    while (data.size() < 128 * 128 * 128)
        data.push_back({pcg32_random_r(&s) / (float)std::numeric_limits<uint32_t>::max(),
                        pcg32_random_r(&s) / (float)std::numeric_limits<uint32_t>::max(),
                        pcg32_random_r(&s) / (float)std::numeric_limits<uint32_t>::max(),
                        pcg32_random_r(&s) / (float)std::numeric_limits<uint32_t>::max()});
    mNoise->bind().setData(GL_RGBA32F, 128, 128, 128, data);
    mNoise->bind().generateMipmaps();
}


void FlowAnimSample::update(float elapsedSeconds)
{
    // IMPORTANT: call base function
    glfw::GlfwApp::update(elapsedSeconds);

    mTime += mAnim * elapsedSeconds;
}

void FlowAnimSample::render(float elapsedSeconds)
{
    GLOW_SCOPED(disable, GL_CULL_FACE);
    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    // GLOW_SCOPED(enable, GL_BLEND);
    // GLOW_SCOPED(blendFunc, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    {
        auto shader = mShader->use();
        shader.setUniform("uWidth", getWindowWidth());
        shader.setUniform("uHeight", getWindowHeight());
        shader.setUniform("uTime", mTime);
        shader.setUniform("uSteps", mSteps);
        shader.setTexture("uNoise", mNoise);

        {
            auto fb = mFBOA->bind();

            static bool first = true;
            if (first)
            {
                glClearColor(0, 0, 0, 1);
                glClear(GL_COLOR_BUFFER_BIT);
                first = false;
            }

            shader.setTexture("uTexture", mResultB);
            mQuad->bind().draw();
        }
        {
            auto fb = mFBOB->bind();
            shader.setTexture("uTexture", mResultA);
            mQuad->bind().draw();
        }
    }

    auto shader = mShaderOutput->use();
    shader.setTexture("uTexture", mResultB);
    mQuad->bind().draw();
}

void FlowAnimSample::onResize(int w, int h)
{
    mResultA->bind().resize(w, h);
    mResultB->bind().resize(w, h);

    mResultA->clear<glm::vec4>({0, 0, 0, 1});
    mResultB->clear<glm::vec4>({0, 0, 0, 1});

    pcg32_random_t s = {17, 171};
    std::vector<glm::vec4> data;
    while ((int)data.size() < w * h)
        data.push_back(glm::vec4(pcg32_random_r(&s) / (float)std::numeric_limits<uint32_t>::max()));
    mResultB->bind().setData(GL_RGBA32F, w, h, data);
}
