#include "shader.hh"

#include "shader-header.glsl"

layout(points) in;
layout(points, max_vertices = 1) out;

in vec4 vPosition[1];
in int vPointID[1];

out uint gPosition;

void main()
{
    vec3 p = vPosition[0].xyz;
    vec3 rp = round(p);
    ivec3 ip = ivec3(rp);
    float dis = distance(p, rp);
    uint udis = dis2udis(dis);
        
    bool isNew = updateDistance(ip, udis, vPointID[0]);

    if (isNew)
    {
        ivec3 is = imageSize(uTexNearestPoint);
        gPosition = (ip.z * is.y + ip.y) * is.x + ip.x;
        EmitVertex();
    }
}
