#version 430 core

in vec2 aPosition;

out vec3 vPosition;

uniform float uCutY;
uniform mat4 uView;
uniform mat4 uProj;

void main() {
    vPosition = vec3(aPosition.x, uCutY - floor(uCutY), aPosition.y);
    
    gl_Position = uProj * uView * vec4(vPosition * 2 - 1, 1.0);
}
