#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class PathTracerSample : public glow::glfw::GlfwApp
{
public:
    PathTracerSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShaderTracer;
    glow::SharedVertexArray mQuad;

    glow::SharedTextureRectangle mBuffer;
    glow::SharedFramebuffer mFramebuffer;
    glow::SharedProgram mShaderOutput;

    int mMaxBounces = 5;
    int mTotalSamples = 0;
    int mSamplesPerFrame = 1;

    int mWidth, mHeight;
    int lastBounces = -1;

    glm::mat4 mLastViewProj;

    void resetTracer();
protected:
    void init() override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;
};
