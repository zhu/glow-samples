#include "GeometryGrassSample.hh"

#include <cmath>
#include <numeric>
#include <random>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <imgui/imgui.h>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/UniformBuffer.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/std140.hh>
#include <glow/util/DefaultShaderParser.hh>

#include <glow-extras/pipeline/RenderPipeline.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include "shader_globals.hh"

#define INT_DIV_CEIL(_x_, _y_) (1 + ((_x_ - 1) / _y_))

using namespace glow;
using namespace glow::camera;

namespace ggrass_detail
{
class PerlinNoise
{
    // The permutation vector
    std::vector<unsigned> p;

public:
    // Initialize with the reference values for the permutation vector
    PerlinNoise()
    {
        // Initialize the permutation vector with the reference values
        p = {151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233, 7,   225, 140, 36,  103, 30,  69,  142, 8,   99,  37,  240,
             21,  10,  23,  190, 6,   148, 247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203, 117, 35,  11,  32,  57,  177, 33,  88,
             237, 149, 56,  87,  174, 20,  125, 136, 171, 168, 68,  175, 74,  165, 71,  134, 139, 48,  27,  166, 77,  146, 158, 231, 83,  111,
             229, 122, 60,  211, 133, 230, 220, 105, 92,  41,  55,  46,  245, 40,  244, 102, 143, 54,  65,  25,  63,  161, 1,   216, 80,  73,
             209, 76,  132, 187, 208, 89,  18,  169, 200, 196, 135, 130, 116, 188, 159, 86,  164, 100, 109, 198, 173, 186, 3,   64,  52,  217,
             226, 250, 124, 123, 5,   202, 38,  147, 118, 126, 255, 82,  85,  212, 207, 206, 59,  227, 47,  16,  58,  17,  182, 189, 28,  42,
             223, 183, 170, 213, 119, 248, 152, 2,   44,  154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,   129, 22,  39,  253, 19,  98,
             108, 110, 79,  113, 224, 232, 178, 185, 112, 104, 218, 246, 97,  228, 251, 34,  242, 193, 238, 210, 144, 12,  191, 179, 162, 241,
             81,  51,  145, 235, 249, 14,  239, 107, 49,  192, 214, 31,  181, 199, 106, 157, 184, 84,  204, 176, 115, 121, 50,  45,  127, 4,
             150, 254, 138, 236, 205, 93,  222, 114, 67,  29,  24,  72,  243, 141, 128, 195, 78,  66,  215, 61,  156, 180};
        // Duplicate the permutation vector
        p.insert(p.end(), p.begin(), p.end());
    }

    // Generate a new permutation vector based on the value of seed
    PerlinNoise(unsigned int seed)
    {
        p.resize(256);

        // Fill p with values from 0 to 255
        std::iota(p.begin(), p.end(), 0);

        // Initialize a random engine with seed
        std::default_random_engine engine(seed);

        // Suffle  using the above random engine
        std::shuffle(p.begin(), p.end(), engine);

        // Duplicate the permutation vector
        p.insert(p.end(), p.begin(), p.end());
    }

    // Get a noise value, for 2D images z can have any value
    float noise(glm::vec3 const& vec) const
    {
        // Find the unit cube that contains the point
        auto X = static_cast<unsigned>(floor(vec.x)) & 255;
        auto Y = static_cast<unsigned>(floor(vec.y)) & 255;
        auto Z = static_cast<unsigned>(floor(vec.z)) & 255;

        float x = vec.x;
        float y = vec.y;
        float z = vec.z;

        // Find relative x, y,z of point in cube
        x -= glm::floor(x);
        y -= glm::floor(y);
        z -= glm::floor(z);

        // Compute fade curves for each of x, y, z
        float u = fade(x);
        float v = fade(y);
        float w = fade(z);

        // Hash coordinates of the 8 cube corners
        auto A = p[X] + Y;
        auto AA = p[A] + Z;
        auto AB = p[A + 1] + Z;
        auto B = p[X + 1] + Y;
        auto BA = p[B] + Z;
        auto BB = p[B + 1] + Z;

        // Add blended results from 8 corners of cube
        auto res = lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z), grad(p[BA], x - 1, y, z)), lerp(u, grad(p[AB], x, y - 1, z), grad(p[BB], x - 1, y - 1, z))),
                        lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1), grad(p[BA + 1], x - 1, y, z - 1)),
                             lerp(u, grad(p[AB + 1], x, y - 1, z - 1), grad(p[BB + 1], x - 1, y - 1, z - 1))));
        return (res + 1.f) / 2.f;
    }

private:
    static float fade(float t) { return t * t * t * (t * (t * 6 - 15) + 10); }
    static float lerp(float t, float a, float b) { return a + t * (b - a); }
    static float grad(unsigned hash, float x, float y, float z)
    {
        int h = hash & 15;
        // Convert lower 4 bits of hash into 12 gradient directions
        float u = h < 8 ? x : y, v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
};

struct GrassBladeVertex
{
    glm::vec3 pos;
    glm::vec3 normal;
    glm::vec3 tangent;

    static std::vector<ArrayBufferAttribute> attributes()
    {
        return {{&GrassBladeVertex::pos, "aPosition"},  //
                {&GrassBladeVertex::normal, "aNormal"}, //
                {&GrassBladeVertex::tangent, "aTangent"}};
    }
};

class GrassField
{
private:
    static auto constexpr bladeDistanceBase = .1f;
    static auto constexpr bladeDistanceVariationBase = .025f;

    static auto constexpr fieldScale = .9f;

    static auto constexpr bladeDistance = bladeDistanceBase * fieldScale;
    static auto constexpr bladeDistanceVariation = bladeDistanceVariationBase * fieldScale;

    struct Section
    {
        glm::vec2 min;
        glm::vec2 max;

        bool contains(glm::vec2 const& point) const { return point.x > min.x && point.x < max.x && point.y > min.y && point.y < max.y; }
    };

private:
    PerlinNoise mNoise;                   ///< Noise generator
    std::vector<Section> mMaskedSections; ///< Masked sections

    glm::vec2 mPosMin = glm::vec2(std::numeric_limits<float>().max()); ///< World space min position of the grass field, Y omitted
    glm::vec2 mPosMax = glm::vec2(std::numeric_limits<float>().min()); ///< World space max position of the grass field, Y omitted
    std::vector<GrassBladeVertex> mBlades;                             ///< Vertices of the point cloud

    glm::ivec2 mTexSize;      ///< Size of the texture
    glm::uvec2 mComputeRange; ///< Amount of workgroups covered by the texture

private:
    void updateTextureSize(float texelSize)
    {
        auto const fieldSizeX = mPosMax.x - mPosMin.x;
        auto const fieldSizeZ = mPosMax.y - mPosMin.y;
        mTexSize = {static_cast<int>(glm::ceil(fieldSizeX / texelSize)), static_cast<int>(glm::ceil(fieldSizeZ / texelSize))};
        mComputeRange = {INT_DIV_CEIL(mTexSize.x, WORK_GROUP_SIZE), INT_DIV_CEIL(mTexSize.y, WORK_GROUP_SIZE)};
    }

    void fill(glm::vec2 const& start, glm::vec2 const& end, float worldHeight, bool performCheck)
    {
        auto const sizeX = static_cast<int>((end.x - start.x) / bladeDistance);
        auto const sizeZ = static_cast<int>((end.y - start.y) / bladeDistance);

        mBlades.reserve(mBlades.size() + static_cast<size_t>(sizeX * sizeZ));
        for (auto x = 0; x < sizeX; ++x)
            for (auto z = 0; z < sizeZ; ++z)
            {
                auto const randomFloatA = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * 2.f - 1.f;
                auto const randomFloatB = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * 2.f - 1.f;

                auto const worldPos
                    = glm::vec3(start.x, worldHeight, start.y)
                      + glm::vec3(x * bladeDistance + bladeDistanceVariation * randomFloatA, 0, z * bladeDistance + bladeDistanceVariation * randomFloatB);

                if (performCheck)
                {
                    bool inAnySection = false;
                    for (auto const& sec : mMaskedSections)
                        if (sec.contains(glm::vec2(worldPos.x, worldPos.z)))
                        {
                            inAnySection = true;
                            break;
                        }

                    if (!inAnySection)
                        continue;
                }

                auto const randomTangent = glm::rotateY(glow::transform::Right(), mNoise.noise(worldPos * 3.f) * glm::pi<float>() * 2.f);

                mBlades.emplace_back(GrassBladeVertex{worldPos, glow::transform::Up(), randomTangent});
            }
    }

public:
    /// Immediately fill a section with grass blades
    /// Faster, use this if the sections are disjoint
    void fillSection(glm::vec2 const& start, glm::vec2 const& end, float worldHeight = 0.f)
    {
        mPosMin = min(start, mPosMin);
        mPosMax = max(end, mPosMax);
        fill(start, end, worldHeight, false);
    }


    /// Add a masked section, will get filled in ::generate
    /// Slower, but prevents overlapping grass sections
    void addMaskedSection(glm::vec2 const& start, glm::vec2 const& end)
    {
        mPosMin = min(start, mPosMin);
        mPosMax = max(end, mPosMax);
        mMaskedSections.emplace_back(Section{start, end});
    }

    /// Generate the point cloud mesh
    SharedVertexArray genMesh(float maskSectionWorldHeight = 0.f)
    {
        if (mMaskedSections.size() > 0)
            fill(mPosMin, mPosMax, maskSectionWorldHeight, true);

        glow::info() << "Generated " << mBlades.size() << " blades of grass";

        auto buffer = ArrayBuffer::create(GrassBladeVertex::attributes(), mBlades);
        return VertexArray::create(buffer, GL_POINTS);
    }

    /// Generate the trample texture
    SharedTexture2D genTexture(float texelSize = .1f)
    {
        updateTextureSize(texelSize);

        glow::info() << "Generated texture of size " << mTexSize.x << "x" << mTexSize.y;
        auto res = Texture2D::createStorageImmutable(mTexSize, GL_RGBA8, 1);
        res->bind().setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
        res->setMipmapsGenerated(true);
        return res;
    }

public:
    GLOW_GETTER(ComputeRange);
    GLOW_GETTER(PosMin);
    GLOW_GETTER(PosMax);
};
}


void GeometryGrassSample::applyConfigToUBO()
{
    mGrassUBOData.cameraUpBias = mConfig.camUpBias;
    mGrassUBOData.windSpeed = mConfig.windSpeed;
    mGrassUBOData.windStrength = mConfig.windStrength;
    mGrassUBOData.bladeThickness = mConfig.bladeThickness;
    mGrassUBOData.bladeLength = mConfig.bladeLength;
}

void GeometryGrassSample::init()
{
    setUsePipeline(true);
    setUsePipelineConfigGui(false);

    GlfwApp::init();

    // Generate grass
    {
        ggrass_detail::GrassField grassField;
        grassField.addMaskedSection(glm::vec2(0, 0), glm::vec2(5, 5));
        grassField.addMaskedSection(glm::vec2(2, 2), glm::vec2(13, 13));
        grassField.addMaskedSection(glm::vec2(10, 0), glm::vec2(15, 5));
        grassField.addMaskedSection(glm::vec2(0, 10), glm::vec2(5, 15));
        grassField.addMaskedSection(glm::vec2(10, 10), glm::vec2(15, 15));
        grassField.addMaskedSection(glm::vec2(17, 17), glm::vec2(22, 22));
        grassField.addMaskedSection(glm::vec2(40, 0), glm::vec2(45, 5));

        mPlane = grassField.genMesh();
        mTextureTrample = grassField.genTexture();

        mTexTrampleComputeRange = grassField.getComputeRange();
        mGrassUBOData.setWorldBounds(grassField.getPosMin(), grassField.getPosMax());
        mGrassUBOData.inverseTrampleTexSize = 1.f / glm::vec2(mTextureTrample->getSize());
    }

    // Create UBO + SSBO
    {
        applyConfigToUBO();
        mGrassUBO = UniformBuffer::create();
        mGrassUBO->bind().setData(mGrassUBOData);


        mTrampleSSBOData.mowerPosX = 0;
        mTrampleSSBOData.mowerPosY = 0;
        //        mTrampleSSBOData.mowerPos = glm::vec2(0);

        static auto constexpr mowerRange = .75f;
        mTrampleSSBOData.mowerRangeSquared = mowerRange * mowerRange;
        mTrampleSSBO = ShaderStorageBuffer::create();
        mTrampleSSBO->bind().setData(mTrampleSSBOData);
    }

    // Load assets
    {
        DefaultShaderParser::addIncludePath(util::pathOf(__FILE__));
        mShaderGrass = Program::createFromFile("grass");
        mShaderGrass->setUniformBuffer("uGrassUBO", mGrassUBO);
        mShaderGrass->setWarnOnUnchangedUniforms(false);

        mShaderTrample = Program::createFromFile("trample.csh");
        mShaderTrample->setUniformBuffer("uGrassUBO", mGrassUBO);
        mShaderTrample->setShaderStorageBuffer("sTrampleInfo", mTrampleSSBO);
        mShaderTrample->setWarnOnUnchangedUniforms(false);

        mShaderClear = Program::createFromFile("clear.csh");

        mTextureNoise = Texture2D::createFromFile(util::pathOf(__FILE__) + "/noise.png", ColorSpace::Linear);
    }

    // Clear trample texture
    {
        auto shader = mShaderClear->use();
        shader.setImage(0, mTextureTrample);
        shader.compute(mTexTrampleComputeRange);
    }

    getCamera()->setLookAt(glm::vec3(0, 2, -1), glm::vec3(3, 0, 3));
    mGpuTimer.init();
}

void GeometryGrassSample::render(float dt)
{
    mMowerPosition += glm::vec2(dt * 0.5f);

    GlfwApp::render(dt);
}

void GeometryGrassSample::onGui()
{
    ImGui::Begin("Grass Config");
    ImGui::SliderFloat("Blade Thickness", &mConfig.bladeThickness, 0.005f, 0.2f);
    ImGui::SliderFloat("Blade Height", &mConfig.bladeLength, 0.1f, 4.f);
    ImGui::SliderFloat("Wind Speed", &mConfig.windSpeed, 0.f, 10.f);
    ImGui::SliderFloat("Wind Strength", &mConfig.windStrength, 0.f, 1.f);
    ImGui::SliderFloat("Cam Up Bias", &mConfig.camUpBias, 0.f, 1.f);
    ImGui::Separator();
    ImGui::Text("Grass drawing: %.4fms", static_cast<double>(mGrassTime));
    ImGui::Separator();
    ImGui::InputFloat2("Mower Position", glm::value_ptr(mMowerPosition));
    if (ImGui::Button("Reset Mower"))
    {
        mMowerPosition = glm::vec2(-0.05f);
    }
    ImGui::SameLine();
    if (ImGui::Button("Refresh grass"))
    {
        // Clear trample texture
        {
            auto shader = mShaderClear->use();
            shader.setImage(0, mTextureTrample);
            shader.compute(mTexTrampleComputeRange);
        }
    }
    ImGui::End();
}

void GeometryGrassSample::onRenderOpaquePass(glow::pipeline::RenderContext const& info)
{
    // UBO + SSBO Update
    {
        mTrampleSSBOData.mowerPosX = mMowerPosition.x;
        mTrampleSSBOData.mowerPosY = mMowerPosition.y;
        mTrampleSSBO->bind().setData(mTrampleSSBOData, GL_STREAM_DRAW);

        applyConfigToUBO();
        mGrassUBOData.cameraUp = getCamera()->handle.getTransform().getUpVector();
        mGrassUBOData.vp = info.camData.vp;
        mGrassUBOData.cleanVp = info.camData.cleanVp;
        mGrassUBOData.prevCleanVp = info.camData.prevCleanVp;
        mGrassUBOData.runtime = getCurrentTime();
        mGrassUBO->bind().setData(mGrassUBOData, GL_STREAM_DRAW);
    }

    // Trampling and mowing
    {
        auto shader = mShaderTrample->use();
        shader.compute(mTexTrampleComputeRange);
    }

    // Grass drawing
    {
        {
            auto timer = mGpuTimer.scope();
            auto shader = info.useProgram(mShaderGrass);

            shader.setTexture("uNoise", mTextureNoise);
            shader.setTexture("uTrampleTexture", mTextureTrample);

            GLOW_SCOPED(disable, GL_CULL_FACE);

            mPlane->bind().draw();
        }
        mGrassTime = mGpuTimer.elapsedSeconds() * 1000.f;
    }
}
