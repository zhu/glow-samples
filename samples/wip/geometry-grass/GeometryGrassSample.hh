#pragma once

#include <vector>

#include <glow/fwd.hh>
#include <glow/std140.hh>

#include <glow-extras/glfw/GlfwApp.hh>
#include <glow-extras/timing/GpuTimer.hh>

namespace ggrass_detail
{
struct GrassUBOData
{
    glow::std140vec4 packedWorldBounds;

    glow::std140vec3 cameraUp;
    glow::std140float cameraUpBias;

    glow::std140float bladeThickness;
    glow::std140float bladeLength;
    glow::std140float windSpeed;
    glow::std140float windStrength;

    glow::std140mat4 vp;
    glow::std140mat4 cleanVp;
    glow::std140mat4 prevCleanVp;

    glow::std140vec2 inverseTrampleTexSize;
    glow::std140float runtime;

    void setWorldBounds(glm::vec2 const& worldMin, glm::vec2 const& worldMax)
    {
        auto const extentX = (worldMax.x - worldMin.x);
        auto const extentY = (worldMax.y - worldMin.y);
        packedWorldBounds = {worldMin.x, extentX, worldMin.y, extentY};
    }
};

struct Trampler
{
    glm::vec2 pos;
    glm::vec2 velocity;
};

struct TrampleSSBOData
{
    float mowerPosX;
    float mowerPosY;
    // glm::vec2 mowerPos;
    float mowerRangeSquared = 1.f;
    // std::vector<Trampler> tramplers;
};
}

class GeometryGrassSample : public glow::glfw::GlfwApp
{
public:
    GeometryGrassSample() : GlfwApp(Gui::ImGui) {}

private:
    ggrass_detail::GrassUBOData mGrassUBOData;
    glow::SharedUniformBuffer mGrassUBO;

    ggrass_detail::TrampleSSBOData mTrampleSSBOData;
    glow::SharedShaderStorageBuffer mTrampleSSBO;

    glow::SharedProgram mShaderGrass;
    glow::SharedProgram mShaderClear;
    glow::SharedProgram mShaderTrample;

    glow::SharedVertexArray mPlane;

    glow::SharedTexture2D mTextureNoise;
    glow::SharedTexture2D mTextureTrample;
    glm::uvec2 mTexTrampleComputeRange;

    glow::timing::GpuTimer mGpuTimer;
    float mGrassTime = 0.f;

    glm::vec2 mMowerPosition = glm::vec2(-0.05f);

    struct
    {
        float bladeThickness = .05f;
        float bladeLength = .85f;
        float windSpeed = 1;
        float windStrength = .25f;
        float camUpBias = .35f;
    } mConfig;

private:
    void applyConfigToUBO();

protected:
    void init() override;
    void render(float elapsedSeconds) override;

    void onGui() override;

    void onRenderOpaquePass(glow::pipeline::RenderContext const& info) override;
};
