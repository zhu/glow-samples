in vec2 aPosition;

out vec2 vTexCoord;

uniform mat4 uModel;

void main() {
    vTexCoord = aPosition;
    gl_Position = uModel * vec4(aPosition, 0.0, 1.0);
    gl_Position.xy = gl_Position.xy * 2 - 1;
}
