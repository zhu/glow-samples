#include "FractalPaintSample.hh"

#include <glow/gl.hh>

#include <algorithm>
#include <vector>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Quad.hh>

#include <GLFW/glfw3.h>

#include <AntTweakBar.h>

using namespace glow;


void FractalPaintSample::drawLine(glm::vec2 from, glm::vec2 to, float pxWidth, glm::vec4 color)
{
    auto quad = mQuad->bind();
    auto shader = mShaderLine->use();

    glm::mat4 m;
    glm::vec2 pxScale = glm::vec2(pxWidth / mWidth, pxWidth / mHeight);
    auto dir = normalize(to - from);
    auto n = glm::vec2(dir.y, -dir.x) * pxScale;
    from -= dir * pxScale;
    to += dir * pxScale;

    m = glm::mat4(glm::vec4{from - n, 0.f, 1.0f}, //
                  glm::vec4{to - n, 0.f, 1.0f},   //
                  glm::vec4{from + n, 0.f, 1.0f}, //
                  glm::vec4{to + n, 0.f, 1.0f});

    shader.setUniform("uModel", m);
    shader.setUniform("uColor", color);
    quad.draw();
}

bool FractalPaintSample::onMousePosition(double x, double y)
{
    if (TwEventMousePosGLFW(window(), x, y))
        return true;

    auto currPos = glm::vec2(x / mWidth, 1 - y / mHeight);

    if (mDrawing)
        mDrawLines.push_back({mLastDrawPos, currPos});

    mLastDrawPos = currPos;

    return true;
}

bool FractalPaintSample::onMouseButton(double x, double y, int button, int action, int mods, int clickCount)
{
    if (TwEventMouseButtonGLFW(window(), button, action, mods))
        return true;

    if (button == GLFW_MOUSE_BUTTON_LEFT)
        mDrawing = action == GLFW_PRESS;

    return true;
}

void FractalPaintSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init();

    mShaderFractal = Program::createFromFile(util::pathOf(__FILE__) + "/fractal");
    mShaderLine = Program::createFromFile(util::pathOf(__FILE__) + "/draw-line");
    mQuad = geometry::Quad<>().generate();

    mCanvas = Texture2D::create(mWidth, mHeight);
    mFramebufferCanvas = Framebuffer::create({{"fColor", mCanvas}});

    mConnectors.push_back({{.3f, .4f}, {.55f, .5f}, {.7f, .4f}});

    TwAddVarRW(tweakbar(), "Depth", TW_TYPE_INT32, &mFractalDepth, "min=0 max=10");
    TwAddVarRW(tweakbar(), "Hue Shift", TW_TYPE_FLOAT, &mHueShift, "min=-90 max=90 step=0.1");
    TwAddVarRW(tweakbar(), "Sat Shift", TW_TYPE_FLOAT, &mSaturationShift, "min=-90 max=90 step=0.1");
    TwAddVarRW(tweakbar(), "Val Shift", TW_TYPE_FLOAT, &mValueShift, "min=-90 max=90 step=0.1");
    TwAddVarRW(tweakbar(), "Pixel Width", TW_TYPE_FLOAT, &mPixelWidth, "min=1 max=20 step=0.1");
    // TwAddVarRW(tweakbar(), "Alpha", TW_TYPE_FLOAT, &mAlpha, "min=0 max=1 step=0.01");
    TwAddVarRW(tweakbar(), "Color", TW_TYPE_COLOR4F, &mColor, "");
    TwAddVarRW(tweakbar(), "Depth on Top", TW_TYPE_BOOLCPP, &mDepthTop, "");
}

void FractalPaintSample::onResize(int w, int h)
{
    mWidth = w;
    mHeight = h;
    mCanvas->bind().resize(w, h);
    mCanvas->clear<glm::vec4>({0, 0, 0, 0});
    mCanvas->bind().generateMipmaps();

    mDrawLines.push_back({mBaseStart, mConnectors[0][0]});
}

void FractalPaintSample::render(float elapsedSeconds)
{
    GLOW_SCOPED(viewport, 0, 0, mWidth, mHeight);
    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);
    GLOW_SCOPED(enable, GL_BLEND);
    GLOW_SCOPED(blendFunc, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClear(GL_COLOR_BUFFER_BIT);

    // update lines
    if (mDrawLines.size() > 0)
    {
        auto fb = mFramebufferCanvas->bind();
        for (auto const& kvp : mDrawLines)
            drawLine(kvp.first, kvp.second, mPixelWidth, mColor);
        mDrawLines.clear();
        mCanvas->bind().generateMipmaps();
    }

    // draw fractal
    {
        auto quad = mQuad->bind();
        auto shader = mShaderFractal->use();
        shader.setTexture("uTexture", mCanvas);

        // gen fractal transformations
        std::vector<std::vector<glm::mat4>> frac;
        std::vector<glm::vec3> hsvshifts;
        frac.push_back({glm::mat4()});
        hsvshifts.push_back(glm::vec3(0));
        std::vector<glm::mat4> conns;
        {
            auto s1 = mBaseStart;
            auto e1 = mBaseEnd;

            auto dir1 = e1 - s1;
            auto n1 = glm::vec2(-dir1.y, dir1.x);

            auto m1 = glm::mat4(glm::vec4(dir1, 0, 0), //
                                glm::vec4(n1, 0, 0),   //
                                glm::vec4(0, 0, 1, 0), //
                                glm::vec4(s1, 0, 1));
            auto im1 = inverse(m1);

            for (auto const& c : mConnectors)
                for (auto i = 0; i < (int)c.size() - 1; ++i)
                {
                    auto s2 = c[i];
                    auto e2 = c[i + 1];

                    auto dir2 = e2 - s2;
                    auto n2 = glm::vec2(-dir2.y, dir2.x);

                    auto m2 = glm::mat4(glm::vec4(dir2, 0, 0), //
                                        glm::vec4(n2, 0, 0),   //
                                        glm::vec4(0, 0, 1, 0), //
                                        glm::vec4(s2, 0, 1));

                    conns.push_back(m2 * im1);
                }
        }
        for (auto d = 0; d < mFractalDepth; ++d)
        {
            std::vector<glm::mat4> ms;
            for (auto const& m : frac.back())
                for (auto const& cm : conns)
                    ms.push_back(cm * m);
            hsvshifts.push_back(glm::vec3(mHueShift, mSaturationShift, mValueShift) * (d + 1));
            frac.push_back(ms);
        }
        if (!mDepthTop)
        {
            std::reverse(frac.begin(), frac.end());
            std::reverse(hsvshifts.begin(), hsvshifts.end());
        }

        // draw it
        auto i = 0u;
        for (auto const& v : frac)
        {
            shader.setUniform("mHsvShift", hsvshifts[i]);
            for (auto const& m : v)
            {
                shader.setUniform("uModel", m);
                quad.draw();
            }

            ++i;
        }
    }

    // draw lines
    drawLine(mBaseStart, mBaseEnd, 3, {1, 0, 0, .5f});
    for (auto const& c : mConnectors)
        for (auto i = 0; i < (int)c.size() - 1; ++i)
            drawLine(c[i], c[i + 1], 3, {0, 0, 1, .5f});
}
