#include "Voxels.hh"

#include <cassert>

#include <array>
#include <utility>

#include <glm/glm.hpp>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/VertexArray.hh>

#include <glow-extras/debugging/DebugRenderer.hh>

#include "VoxelSample.hh"

static const std::array<glm::ivec3, 6> sDirection2Vec3 = {{glm::ivec3(1, 0, 0),  //
                                                           glm::ivec3(-1, 0, 0), //
                                                           glm::ivec3(0, 1, 0),  //
                                                           glm::ivec3(0, -1, 0), //
                                                           glm::ivec3(0, 0, 1),  //
                                                           glm::ivec3(0, 0, -1)}};

Voxels::Voxels(VoxelSample* sample) : sample(sample)
{
    data.resize(Size * Size * Size);

    {
        auto s = Size;
        while (s >= 1)
        {
            for (auto i = 0; i < 6; ++i)
                lightmap[i].push_back(std::vector<LightData>(s * s * s));
            s /= 2;
        }
    }
}

void Voxels::generate()
{
    for (auto z = 0; z < Size; ++z)
        for (auto y = 0; y < Size; ++y)
            for (auto x = 0; x < Size; ++x)
            {
                auto idx = idxOf(x, y, z);

                auto p = glm::vec3(x + .5f, y + .5f, z + .5f);
                auto dis = glm::distance(p, glm::vec3(Size / 2.0f));

                auto& d = data[idx];

                // if (isBoundary(x, y, z) || dis < Size / 2.0f * 1.1f)
                if (y == Size - 1 || (y > 1 && x > 1 && z > 1 && x < Size - 2 && z < Size - 2 && dis < Size / 2.0f * 1.1f))
                {
                    d.mat = 0;
                }
                else
                {
                    d.mat = 1;
                    d.color = glm::vec4(0.6f, 0.6f, 0.6f, 1.0f);
                }
            }
}

void Voxels::updateLighting()
{
    for (auto dir = 0; dir < 6; ++dir)
    {
        auto& map = lightmap[dir];

        // init lvl 0
        {
            auto& lvl0 = map[0];
            for (auto z = 0; z < Size; ++z)
                for (auto y = 0; y < Size; ++y)
                    for (auto x = 0; x < Size; ++x)
                    {
                        auto idx = lightIdxOf(x, y, z, 0);
                        auto& ld = lvl0[idx];
                        auto& d = data[idx];

                        if (d.mat == 0)
                        {
                            ld.emission = glm::vec3(0.0f);
                            ld.opacity = 0.0f;
                        }
                        else
                        {
                            ld.emission = glm::vec3(0.1f); // "dark"
                            ld.opacity = 1.0f;
                        }
                    }

            // sun
            if ((Direction)dir == Direction::PosY)
            {
                for (auto z = 0; z < Size; ++z)
                    for (auto x = 0; x < Size; ++x)
                    {
                        auto y = Size - 1;
                        while (y >= 0 && data[idxOf(x, y, z)].mat == 0)
                            --y;

                        if (y >= 0)
                            lvl0[idxOf(x, y, z)].emission = glm::vec3(1.0);
                    }
            }

            // sky
            // TODO: inject into border
        }

        // generate mipmaps
        {
            auto l = 1;
            auto s = Size / 2;
            while (s > 0)
            {
                auto& lvlCurr = map[l];
                auto& lvlPrev = map[l - 1];

                auto sx = lightStepX(l - 1);
                auto sy = lightStepY(l - 1);
                auto sz = lightStepZ(l - 1);

                switch ((Direction)dir)
                {
                case Direction::PosX:
                case Direction::NegX:
                    // nothing
                    break;
                case Direction::PosY:
                case Direction::NegY:
                    std::swap(sx, sy);
                    break;
                case Direction::PosZ:
                case Direction::NegZ:
                    std::swap(sx, sz);
                    break;
                }

                // TODO: test me
                auto mFirst = 1;
                auto mSecond = 0;
                if (dir % 2 == 1)
                    std::swap(mFirst, mSecond);

                for (auto z = 0; z < s; ++z)
                    for (auto y = 0; y < s; ++y)
                        for (auto x = 0; x < s; ++x)
                        {
                            auto& d = lvlCurr[lightIdxOf(x, y, z, l)];

                            d.opacity = 0.0;
                            d.emission = glm::vec3(0.0f);

                            // "sx" is always in direction

                            auto bi = lightIdxOf(x * 2, y * 2, z * 2, l - 1);
                            for (auto dy = 0; dy < 2; ++dy)
                                for (auto dz = 0; dz < 2; ++dz)
                                {
                                    auto& dFirst = lvlPrev[bi + mFirst * sx + dy * sy + dz * sz];
                                    auto& dSecond = lvlPrev[bi + mSecond * sx + dy * sy + dz * sz];

                                    auto opacity = dFirst.opacity + dSecond.opacity;
                                    auto emission = dFirst.emission + dSecond.emission;

                                    if (opacity > 1.0f)
                                    {
                                        opacity = 1.0f;
                                        // if first opacity is 1.0, only first emission counts
                                        emission = dFirst.emission + dSecond.emission * (1.0f - dFirst.opacity);
                                    }

                                    d.opacity += opacity / 4.0f;
                                    d.emission += emission / 4.0f;
                                }
                        }

                ++l;
                s /= 2;
            }
        }
    }
}

glm::vec3 Voxels::lightingAt(int idx, int sdx, int sdy, int sn, Direction dir) const
{
    glm::vec3 light;

    auto isCorner = data[idx + sn + sdx].mat != 0 && data[idx + sn + sdy].mat != 0;

    // emission/direct light
    {
        auto& map = lightmap[(int)dir][0];

        auto c00 = map[idx].emission;
        auto c01 = map[idx + sdy].emission;
        auto c10 = map[idx + sdx].emission;
        auto c11 = map[idx + sdx + sdy].emission;
        auto o00 = map[idx].opacity;
        auto o01 = map[idx + sdy].opacity;
        auto o10 = map[idx + sdx].opacity;
        auto o11 = map[idx + sdx + sdy].opacity;
        // AO corner case
        if (isCorner)
        {
            o11 = 0.0f;
            c11 = glm::vec3(0.0f);
        }
        assert(o00 + o01 + o10 + o11 >= 1.0f && "direct emission should be at least one visible");
        light += (c00 + c01 + c10 + c11) / glm::max(0.01f, o00 + o01 + o10 + o11);
    }

    // indirect diffuse bounce
    {
        auto d = sDirection2Vec3[(int)dir];
        auto dx = stepToDir(sdx);
        auto dy = stepToDir(sdy);
        auto pos = idxToPos(idx);

        // debug
        auto qc = glm::vec3(pos) + 0.5f + glm::vec3(d) * 0.5f;
        auto rp = glm::vec3(pos) + 0.5f + glm::vec3(d + dx + dy) * 0.5f;
        (void)rp;
        auto sdir = sample->getSelectedPos() - qc;
        auto debug = sample->getSelectedCube() == pos && //
                     dot(sdir, glm::vec3(dx)) > 0 &&  //
                     dot(sdir, glm::vec3(dy)) > 0;
        auto ad = abs(sample->getSelectedPos() - (glm::vec3(pos) + 0.5f));
        auto am = glm::max(ad.x, glm::max(ad.y, ad.z));
        if (am == ad.x && dir != Direction::PosX)
            debug = false;
        if (am == ad.y && dir != Direction::PosY)
            debug = false;
        if (am == ad.z && dir != Direction::PosZ)
            debug = false;
        if (am == -ad.x && dir != Direction::NegX)
            debug = false;
        if (am == -ad.y && dir != Direction::NegY)
            debug = false;
        if (am == -ad.z && dir != Direction::NegZ)
            debug = false;

        // 4 top cones
        const float contributionTop = 1 / 4.0f; // TODO
        light += contributionTop * traceCone(dir, pos + d, d, -dx, -dy, debug ? 1 : 0);
        light += contributionTop * traceCone(dir, pos + d + dx, d, dx, -dy, debug ? 2 : 0);
        light += contributionTop * traceCone(dir, pos + d + dy, d, -dx, dy, debug ? 3 : 0);
        if (!isCorner)
            light += contributionTop * traceCone(dir, pos + d + dy + dx, d, dx, dy, debug ? 4 : 0);

        // 8 side cones
        // TODO
    }

    return light;
}

static int dot(glm::ivec3 a, glm::ivec3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

glm::vec3 Voxels::traceCone(Direction ldir, glm::ivec3 pos, glm::ivec3 dir, glm::ivec3 dx, glm::ivec3 dy, int debug) const
{
    return {0,0,0};
    // current slice goes from pos to pos + distance * (dx + dy) + size * dir
    int size = 1;
    int distance = 1;
    int lightLvl = 0;
    glm::vec4 color(debug % 2 == 0, debug % 4 < 2, 1.0, 1.0f);

    float currOpacity = 0.0f;
    glm::vec3 currEmission = glm::vec3(0.0);

    while (true)
    {
        /*if (debug == 1)
            glow::info() << "tracing " << to_string(pos) << ", dis " << distance << ", size " << size;

        // collect slice
        if (debug > 0)
        {
            glm::vec3 offset(0.03f);
            glm::vec3 amin = glm::vec3(pos);
            glm::vec3 amax = glm::vec3(pos);
            for (auto const& p : {glm::vec3(pos + 1), glm::vec3(pos + 1 + (size - 1) * dir + (distance - 1) * (dx + dy)),
                           glm::vec3(pos + (size - 1) * dir + (distance - 1) * (dx + dy))})
            {
                amin = glm::min(amin, p);
                amax = glm::max(amax, p);
            }
            sample->debug()->renderAABB(amin + offset - Size / 2.0f, amax - offset - Size / 2.0f, color, true);
        }*/
        
        auto start = glm::max(glm::ivec3(0), glm::min(pos, pos + (distance - 1) * (dx + dy) + (size - 1) * dir));
        auto end = glm::min(glm::ivec3(Size - 1), glm::max(pos, pos + (distance - 1) * (dx + dy) + (size - 1) * dir));
        auto adx = abs(dx);
        auto ady = abs(dy);
        // adjust start
        auto adjstart = start;
        adjstart -= adx * (dot(start, adx) % size);
        adjstart -= ady * (dot(start, ady) % size);
        auto cx = (dot(adx, end - adjstart)) / size;
        auto cy = (dot(ady, end - adjstart)) / size;

        if (debug == 1)
            glow::info() << "  cx " << cx << ", cy " << cy;

        auto sall = end - start + 1;
        auto volAll = sall.x * sall.y * sall.z;
        auto volSum = 0;
        glm::vec3 emission = glm::vec3(0.0f);
        float opacity = 0.0f;
        auto const& lmap = lightmap[(int)ldir][lightLvl];
        for (auto iy = 0; iy <= cy; ++iy)
            for (auto ix = 0; ix <= cx; ++ix)
            {
                auto p = adjstart + size * (adx * ix + ady * iy);
                assert(p.x % size == 0);
                assert(p.y % size == 0);
                assert(p.z % size == 0);

                auto smin = glm::max(p, start);
                auto smax = glm::min(p + size - 1, end);
                assert(glm::all(glm::lessThanEqual(smin, smax)));
                auto ss = smax - smin + 1;
                auto vol = ss.x * ss.y * ss.z;
                volSum += vol;

                auto const& ld = lmap[lightIdxOf(p.x / size, p.y / size, p.z / size, lightLvl)];
                float ratio = vol / (float)volAll;
                emission += ld.emission * ratio;
                opacity += ld.opacity * ratio;

                // if (debug == 1)
                // {
                //     glm::vec3 offset(0.1f);
                //     app->debug()->renderAABB(glm::vec3(p) + offset - Size / 2.0f,
                //                              glm::vec3(p + size) - offset - Size / 2.0f, glm::vec4(0, 1, 0, 0.2f));
                // }
            }
        if (volSum != volAll)
        {
            glow::info() << "tracing " << to_string(pos) << ", dis " << distance << ", size " << size;
            glow::info() << "  cx " << cx << ", cy " << cy;
            glow::info() << "  from " << to_string(start) << " to " << to_string(end);
            glow::info() << "  adj from " << to_string(adjstart);
            glow::info() << "VOLUME MISMATCH: " << volSum << " vs. predicted " << volAll;
        }
        assert(volSum == volAll);

        // volume rendering
        // currOpacity += (1 - currOpacity) * opacity;
        // pessimistic lighting:
        if (currOpacity + opacity <= 1.0)
            currEmission += emission;
        else
            currEmission += emission * (1.0 - currOpacity);
        currOpacity += opacity;

        if (currOpacity >= 0.99)
            break; // solid

        // advance
        pos += dir * size;
        distance += size;

        if (!contains(pos))
            break; // out of bounds

        // increase on "click"
        if (dot(start, dir) % (size * 2) == 0)
        {
            size *= 2;
            ++lightLvl;
        }
    }

    return currEmission; // TODO
}

glow::SharedVertexArray Voxels::buildMesh(glow::SharedShaderStorageBuffer const& voxelData) const
{
    std::vector<VoxelVertex> vertices;
    std::vector<int> indices;

    std::array<int, 6> dirStepFull = {{StepX, -StepX, StepY, -StepY, StepZ, -StepZ}};
    std::array<int, 5> dirStep = {{StepX, StepY, StepZ, StepX, StepY}};
    std::array<glm::vec3, 5> dirs;
    for (auto i = 0u; i < dirs.size(); ++i)
        dirs[i] = glm::vec3(i % 3 == 0, i % 3 == 1, i % 3 == 2);

    auto aoAt = [this](int idx, int sdx, int sdy) -> int
    {
        auto const& ds1 = data[idx + sdx];
        auto const& ds2 = data[idx + sdy];

        if (ds1.mat != 0 && ds2.mat != 0)
            return 1;

        auto const& e = data[idx + sdx + sdy];

        return 4 - (ds1.mat != 0) - (ds2.mat != 0) - (e.mat != 0);
    };

    std::vector<GpuVoxel> voxels;

    for (auto z = 1; z < Size - 1; ++z)
        for (auto y = 1; y < Size - 1; ++y)
            for (auto x = 1; x < Size - 1; ++x)
            {
                auto idx = idxOf(x, y, z);

                if (data[idx].mat == 0)
                    continue;

                auto p = glm::vec3(x, y, z) + 0.5f;

                auto const& dThis = data[idx];

                for (auto dir = 0; dir < 6; ++dir)
                {
                    auto ni = idx + dirStepFull[dir];
                    auto const& dThat = data[ni];

                    // solid -> air
                    if (dThat.mat == 0)
                    {
                        auto n = dirs[dir / 2];
                        auto dx = dirs[dir / 2 + 1];
                        auto dy = dirs[dir / 2 + 2];

                        auto sn = dirStep[dir / 2 + 0];
                        auto sdx = dirStep[dir / 2 + 1];
                        auto sdy = dirStep[dir / 2 + 2];

                        if (dir % 2 == 1)
                        {
                            std::swap(dx, dy);
                            std::swap(sdx, sdy);

                            n *= -1.0f;

                            dx *= -1.0f;
                            dy *= -1.0f;

                            sdx *= -1;
                            sdy *= -1;
                        }

                        auto bi = vertices.size();

                        // v00
                        // v01
                        // v10
                        // v11

                        std::array<float, 4> ao;

                        glm::mat4 colors;
                        glm::mat4 lights;

                        for (auto vi = 0; vi < 4; ++vi)
                        {
                            VoxelVertex v;
                            v.idx = voxels.size();
                            v.uv = glm::vec2(vi > 1, vi % 2);

                            v.pos = p + n / 2 - dx / 2 - dy / 2;
                            if (vi % 2 == 1)
                                v.pos += dy;
                            if (vi > 1)
                                v.pos += dx;

                            colors[vi] = dThis.color;
                            lights[vi] = glm::vec4(
                                lightingAt(idx, vi > 1 ? sdx : -sdx, vi % 2 == 1 ? sdy : -sdy, sn, (Direction)dir),
                                ao[vi] = aoAt(ni, vi > 1 ? sdx : -sdx, vi % 2 == 1 ? sdy : -sdy) / 4.0f);

                            vertices.push_back(v);
                        }

                        GpuVoxel voxel;
                        voxel.normal = n;
                        voxel.colors = colors;
                        voxel.lights = lights;
                        voxels.push_back(voxel);

                        indices.push_back(bi + 1);
                        indices.push_back(bi + 2);
                        indices.push_back(bi + 3);

                        indices.push_back(bi + 1);
                        indices.push_back(bi + 0);
                        indices.push_back(bi + 2);
                    }
                }
            }

    voxelData->bind().setData(voxels);

    auto ab = glow::ArrayBuffer::create({
        {&VoxelVertex::uv, "aUV"},        //
        {&VoxelVertex::pos, "aPosition"}, //
        {&VoxelVertex::idx, "aIdx"},      //
    });
    ab->bind().setData(vertices);

    auto eab = glow::ElementArrayBuffer::create(indices);

    return glow::VertexArray::create(ab, eab);
}
