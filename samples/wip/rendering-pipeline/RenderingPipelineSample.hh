#pragma once

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include <glow-extras/debugging/DebugRenderer.hh>

#include <glow-extras/pipeline/RenderCallback.hh>
#include <glow-extras/pipeline/RenderPipeline.hh>
#include <glow-extras/pipeline/RenderScene.hh>

class RenderingPipelineSample : public glow::glfw::GlfwApp
{
public:
    RenderingPipelineSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    struct MeshEntry
    {
        glow::SharedVertexArray mesh;
        glm::mat4 model;
        glm::vec3 albedo;
        float roughness;
        float metallic;
    };

    struct TransparentMeshEntry
    {
        glm::mat4 model;
        glm::vec3 albedo;
        float roughness;
        float metallic;
        float alpha;
    };

private:
    static constexpr float sampleRoughnessDist[] = {0.15f, .2f, .25f, .3f, .4f, .5f, .6f, .7f, .8f, .9f, 1.f};
    static constexpr float sampleMetallicDist[] = {0.f, .1f, .5f, .9f, 1.f};
    static constexpr float sampleRoughnessPosOffset = 2.7f;
    static constexpr float sampleMetallicPosOffset = 2.5f;

    static constexpr float sampleMaxX = sampleRoughnessPosOffset * ((sizeof(sampleRoughnessDist) / sizeof(float)) - 1);
    static constexpr float sampleMaxZ = sampleMetallicPosOffset * ((sizeof(sampleMetallicDist) / sizeof(float)) - 1);

    // == Shaders ==
    glow::SharedProgram mShaderDepthPre;
    glow::SharedProgram mShaderOpaqueForward;
    glow::SharedProgram mShaderTransparent;

    // == Meshes ==
    glow::SharedVertexArray mMeshSample;
    std::vector<MeshEntry> mMeshEntries;
    std::vector<TransparentMeshEntry> mTransparentMeshEntries;

    // == IBL maps ==
    glow::SharedTextureCubeMap mSkyboxMap;
    glow::SharedTextureCubeMap mEnvironmentMap;


private:
    // Config
    bool mDebugShowClusterHeatmap = false;
    bool mDebugShowClusterCoords = false;

    // Lights
    glow::pipeline::SharedLight mMovingLight;

    void initLights();
    void updateLights();

    // Meshes
    void initMeshes();
    void drawOpaqueGeometry(glow::UsedProgram& shader, glow::pipeline::CameraData const& camData, bool zPreOnly = false);

protected:
    void init() override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;
    void onGui() override;

protected:
    // == Render callback overrides ==
    void onPerformShadowPass(glow::pipeline::RenderScene const&,
                             glow::pipeline::RenderStage const&,
                             glow::pipeline::CameraData const&,
                             glow::UsedProgram&) override;

    void onRenderDepthPrePass(glow::pipeline::RenderContext const& info) override;
    void onRenderOpaquePass(glow::pipeline::RenderContext const& info) override;
    void onRenderTransparentPass(glow::pipeline::RenderContext const& info) override;

public:
    ~RenderingPipelineSample() override {}
};
