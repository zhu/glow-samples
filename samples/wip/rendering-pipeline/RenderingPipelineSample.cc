#include "RenderingPipelineSample.hh"

#include <cmath>
#include <memory>
#include <vector>

#include <glm/ext.hpp>

#include <imgui/imgui.h>

#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/util/DefaultShaderParser.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/colors/color.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/material/IBL.hh>
#include <glow-extras/pipeline/RenderScene.hh>
#include <glow-extras/pipeline/lights/Light.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

using namespace glow;
using namespace glow::camera;
using namespace glow::pipeline;

constexpr float RenderingPipelineSample::sampleRoughnessDist[];
constexpr float RenderingPipelineSample::sampleMetallicDist[];

void RenderingPipelineSample::onResize(int w, int h)
{
    getCamera()->setViewportSize(w, h);
}

void RenderingPipelineSample::onGui()
{
    ImGui::SetNextWindowPos(ImVec2(390, 20), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(200, 200), ImGuiCond_FirstUseEver);

    ImGui::Begin("Pipeline Sample");

    ImGui::Checkbox("Light coverage heatmap", &mDebugShowClusterHeatmap);
    ImGui::Checkbox("Cluster debug colors", &mDebugShowClusterCoords);

    static bool rejectionEnabled = getPipelineCamera()->isRejectingHistory();
    ImGui::Checkbox("Reject TAA History", &rejectionEnabled);
    getPipelineCamera()->setRejectHistory(rejectionEnabled);

    //    if (ImGui::Button("Redraw shadows"))
    //    {
    //        getPipelineScene()->shadowMode = ShadowMode::UpdateOnce;
    //    }

    if (ImGui::Button("Reallocate targets"))
    {
        getPipeline()->freeAllTargets();
    }

    if (ImGui::Button("Reset Camera"))
    {
        getCamera()->setLookAt(glm::vec3(sampleMaxX - 4, 5, sampleMaxZ + 10), glm::vec3(sampleMaxX / 2, -3, sampleMaxZ / 2));
    }

    ImGui::End();
}

void RenderingPipelineSample::onPerformShadowPass(const RenderScene&, const RenderStage&, const CameraData& d, UsedProgram& shader)
{
    drawOpaqueGeometry(shader, d, true);
}

void RenderingPipelineSample::onRenderDepthPrePass(glow::pipeline::RenderContext const& info)
{
    auto shader = info.useProgram(mShaderDepthPre);

    shader.setUniform("uView", info.camData.view);
    shader.setUniform("uProj", info.camData.proj);

    drawOpaqueGeometry(shader, info.camData, true);
}

void RenderingPipelineSample::onRenderOpaquePass(glow::pipeline::RenderContext const& info)
{
    auto shader = info.useProgram(mShaderOpaqueForward);

    shader.setUniform("uView", info.camData.view);
    shader.setUniform("uProj", info.camData.proj);
    shader.setUniform("uCamPos", info.camData.camPos);
    material::IBL::prepareShaderGGX(shader, mEnvironmentMap);

    shader.setUniform("uSunDirection", info.scene.sunDirection);
    shader.setUniform("uSunColor", info.scene.sunColor * info.scene.sunIntensity);

    // Debug
    shader.setUniform("uShowClusterHeatmap", mDebugShowClusterHeatmap);
    shader.setUniform("uShowClusterIndexHash", mDebugShowClusterCoords);

    drawOpaqueGeometry(shader, info.camData, false);
}

void RenderingPipelineSample::onRenderTransparentPass(glow::pipeline::RenderContext const& info)
{
    auto shader = info.useProgram(mShaderTransparent);

    shader.setUniform("uView", info.camData.view);
    shader.setUniform("uProj", info.camData.proj);
    shader.setUniform("uCamPos", info.camData.camPos);
    shader.setUniform("uSunDirection", info.scene.sunDirection);
    shader.setUniform("uSunColor", info.scene.sunColor);

    for (auto const& mesh : mTransparentMeshEntries)
    {
        shader.setUniform("uModel", mesh.model);

        shader.setUniform("uRoughness", mesh.roughness);
        shader.setUniform("uMetallic", mesh.metallic);
        shader.setUniform("uAlbedo", mesh.albedo);
        shader.setUniform("uMaterialAlpha", mesh.alpha);

        mMeshSample->bind().draw();
    }
}

void RenderingPipelineSample::initLights()
{
    static constexpr auto radius = 7.f;
    static constexpr auto size = 1.5f;
    static constexpr auto defY = 3.5f;

    static constexpr auto intensity = 1.5f;
    static constexpr auto roughIntensity = 4.f;
    static const auto backLeftC = colors::color::from_hex("CEF19E");
    static const auto frontLeftC = colors::color::from_hex("836890");
    static const auto centerC = colors::color::from_hex("0DFF9F");
    static const auto backRightC = colors::color::from_hex("068587");
    static const auto frontRightC = colors::color::from_hex("ED553B");
    static const auto movingC = colors::color::from_hex("ED553B");

    auto& scene = *getPipelineScene();
    scene.lights.push_back(std::make_shared<glow::pipeline::Light>(glm::vec3(1, defY, 1), backLeftC.to_rgb() * intensity, size, radius));
    scene.lights.push_back(std::make_shared<glow::pipeline::Light>(glm::vec3(1, defY, sampleMaxZ - 1), frontLeftC.to_rgb() * intensity, size, radius));
    scene.lights.push_back(std::make_shared<glow::pipeline::Light>(glm::vec3(sampleMaxX - 1, defY, 1), backRightC.to_rgb() * roughIntensity, size, radius));
    scene.lights.push_back(
        std::make_shared<glow::pipeline::Light>(glm::vec3(sampleMaxX / 2, defY + 2, sampleMaxZ / 2), centerC.to_rgb() * roughIntensity, size, radius));
    scene.lights.push_back(
        std::make_shared<glow::pipeline::Light>(glm::vec3(sampleMaxX - 1, defY, sampleMaxZ - 1), frontRightC.to_rgb() * roughIntensity, size, radius));

    // Moving Light
    scene.lights.push_back(mMovingLight = std::make_shared<glow::pipeline::Light>(glm::vec3(0, defY, 0), movingC.to_rgb() * intensity, size, 4.f));


    // Long Tube lights
    static constexpr auto tubeLightY = -7;
    scene.lights.push_back(std::make_shared<glow::pipeline::Light>(glm::vec3(1, tubeLightY, sampleMaxZ - 1), glm::vec3(sampleMaxX - 1, tubeLightY, 1),
                                                                   frontLeftC.to_rgb(), .25f, radius));
    scene.lights.push_back(std::make_shared<glow::pipeline::Light>(glm::vec3(1, tubeLightY, 1), glm::vec3(sampleMaxX - 1, tubeLightY, sampleMaxZ - 1),
                                                                   backRightC.to_rgb(), .25f, radius));
}

void RenderingPipelineSample::updateLights()
{
    const auto timeVaryingX = (sampleMaxX / 2) + std::sin(getCurrentTime()) * (sampleMaxX / 2);
    const auto timeVaryingZ = (sampleMaxZ / 2) + std::cos(getCurrentTime()) * (sampleMaxZ / 2);

    mMovingLight->setPosition(glm::vec3(timeVaryingX, 3.5f, timeVaryingZ));
}

void RenderingPipelineSample::initMeshes()
{
    // Monkeys
    int roughnessCount = 0;
    for (float roughness : sampleRoughnessDist)
    {
        int metallicCount = 0;
        for (float metallic : sampleMetallicDist)
        {
            mMeshEntries.emplace_back(MeshEntry{
                mMeshSample, glm::translate(glm::vec3(roughnessCount * sampleRoughnessPosOffset, 0, metallicCount * sampleMetallicPosOffset)),
                glm::vec3(1, .9f, 1), roughness, metallic});

            mTransparentMeshEntries.emplace_back(
                TransparentMeshEntry{glm::translate(glm::vec3(roughnessCount * sampleRoughnessPosOffset, -4, metallicCount * sampleMetallicPosOffset)),
                                     glm::vec3(.6f, .6f, 1), roughness, metallic, std::max(roughness, 0.1f)});

            ++metallicCount;
        }
        ++roughnessCount;
    }

    // Floor
    mMeshEntries.emplace_back(MeshEntry{glow::geometry::Cube<>().generate(),
                                        glm::translate(glm::vec3(sampleMaxX / 2, -8, sampleMaxZ / 2)) * glm::scale(glm::vec3(sampleMaxX, .25f, sampleMaxZ)),
                                        glm::vec3(.75), .45f, .95f});
}

void RenderingPipelineSample::drawOpaqueGeometry(UsedProgram& shader, const CameraData& camData, bool zPreOnly)
{
    for (auto const& mesh : mMeshEntries)
    {
        shader.setUniform("uModel", mesh.model);

        if (!zPreOnly)
        {
            const auto cleanMvp = camData.cleanVp * mesh.model;
            const auto prevCleanMvp = camData.prevCleanVp * mesh.model; // Note that this would have to be previousModel if the objects moved


            shader.setUniform("uCleanMvp", cleanMvp);
            shader.setUniform("uPrevCleanMvp", prevCleanMvp);

            shader.setUniform("uRoughness", mesh.roughness);
            shader.setUniform("uMetallic", mesh.metallic);
            shader.setUniform("uAlbedo", mesh.albedo);
        }

        mesh.mesh->bind().draw();
    }
}

void RenderingPipelineSample::init()
{
    // -- GlfwApp Init --
    {
        setUsePipeline(true);
        setGui(GlfwApp::Gui::ImGui);
        setUsePipelineConfigGui(true);

        GlfwApp::init();
    }

    // -- Shader Include Setup --
    {
        // Initialize Material shaders
        glow::material::IBL::GlobalInit();
    }

    // -- Sample-specific configuration --
    {
        getPipelineScene()->sunIntensity = .15f;
        getPipelineScene()->contrast = 1.3f;
        getCamera()->setFarPlane(1000);
        getCamera()->setLookAt(glm::vec3(sampleMaxX - 4, 5, sampleMaxZ + 10), glm::vec3(sampleMaxX / 2, -3, sampleMaxZ / 2));
    }

    // -- Asset loading --
    {
        DefaultShaderParser::addIncludePath(util::pathOf(__FILE__) + "/shaders");

        mShaderDepthPre = Program::createFromFiles({"depthPre/geometry.vsh", "depthPre/depthPre.fsh"});
        mShaderOpaqueForward = Program::createFromFiles({"geometry.vsh", "opaque/litOpaque.fsh"});
        mShaderTransparent = Program::createFromFiles({"geometry.vsh", "transparent/oit.fsh"});

        const std::string dataPath = util::pathOf(__FILE__) + "/../../../data/";

        mMeshSample = assimp::Importer().load(dataPath + "suzanne.obj");

        const auto skyboxPath = dataPath + "ibl/studio/skybox/skybox_";
        mSkyboxMap = TextureCubeMap::createFromData(TextureData::createFromFileCube(skyboxPath + "posx.hdr", skyboxPath + "negx.hdr",
                                                                                    skyboxPath + "posy.hdr", skyboxPath + "negy.hdr",
                                                                                    skyboxPath + "posz.hdr", skyboxPath + "negz.hdr", ColorSpace::sRGB));
    }

    // -- IBL setup --
    {
        mEnvironmentMap = material::IBL::createEnvMapGGX(mSkyboxMap, 1024);

        auto usedShader = mShaderOpaqueForward->use();
        material::IBL::initShaderGGX(usedShader);
    }

    // -- Sample scene init --
    {
        initLights();
        initMeshes();
    }
}

void RenderingPipelineSample::render(float dt)
{
    updateLights();
    GlfwApp::render(dt);
}
