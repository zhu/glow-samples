uniform float uGrassDepth;
uniform float uVoxelWidth;
uniform float uVolumeSize;
uniform vec3 uDepthDir;

uniform layout(r32i, binding=0) iimage3D uVolumeR;
uniform layout(r32i, binding=1) iimage3D uVolumeG;
uniform layout(r32i, binding=2) iimage3D uVolumeB;
uniform layout(r32i, binding=3) iimage3D uVolumeA;
uniform layout(r32i, binding=4) iimage3D uVolumeDX;
uniform layout(r32i, binding=5) iimage3D uVolumeDY;
uniform layout(r32i, binding=6) iimage3D uVolumeDZ;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vColor;

out vec4 fColor;

void main() {
    vec3 pos = vPosition;
    ivec3 ipos = ivec3((pos * uVolumeSize));
    //ipos.xy = ivec2(gl_FragCoord.xy);

    // if valid, write
    if (ipos.x >= 0 &&
        ipos.y >= 0 &&
        ipos.z >= 0 &&
        ipos.x < uVolumeSize &&
        ipos.y < uVolumeSize &&
        ipos.z < uVolumeSize )
    {
        float a = uGrassDepth / uVoxelWidth; // approximation
        a *= abs(dot(uDepthDir, vNormal)); // modulation by "side"
        int ia = int(a * 1000);
        int ir = int(vColor.r * 1000);
        int ig = int(vColor.g * 1000);
        int ib = int(vColor.b * 1000);

        // color premultiplied
        imageAtomicAdd(uVolumeR, ipos, ir * ia);
        imageAtomicAdd(uVolumeG, ipos, ig * ia);
        imageAtomicAdd(uVolumeB, ipos, ib * ia);

        // direction
        imageAtomicExchange(uVolumeDX, ipos, int(vTangent.x * 1000) * ia);
        imageAtomicExchange(uVolumeDY, ipos, int(vTangent.y * 1000) * ia);
        imageAtomicExchange(uVolumeDZ, ipos, int(vTangent.z * 1000) * ia);

        // alpha direct
        imageAtomicAdd(uVolumeA, ipos, ia);
    }

    fColor = vec4(0, 1, 0, 1); // DUMMY
}
