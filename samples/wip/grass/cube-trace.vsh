in vec3 aPosition;

out vec3 vWorldSpacePos;

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uModel;

void main() {
    vWorldSpacePos = vec3(uModel * vec4(aPosition, 1.0));
    gl_Position = uProj * uView * uModel * vec4(aPosition, 1.0);
}
