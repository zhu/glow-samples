layout(local_size_x = 2, local_size_y = 2, local_size_z = 2) in;

uniform layout(r32i, binding=0) iimage3D uVolumeDX;
uniform layout(r32i, binding=1) iimage3D uVolumeDY;
uniform layout(r32i, binding=2) iimage3D uVolumeDZ;
uniform layout(r32i, binding=3) iimage3D uVolumeA;
uniform layout(rgba16f, binding=4) writeonly image3D uOut;

void main()
{
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint z = gl_GlobalInvocationID.z;

    ivec3 s = imageSize(uOut);

    if (x >= s.x || y >= s.y || z >= s.z)
        return; // out of bounds

    vec4 data;
    data.x = float(imageLoad(uVolumeDX, ivec3(x, y, z))) / (1000.f * 1000.f);
    data.y = float(imageLoad(uVolumeDY, ivec3(x, y, z))) / (1000.f * 1000.f);
    data.z = float(imageLoad(uVolumeDZ, ivec3(x, y, z))) / (1000.f * 1000.f);
    data.a = float(imageLoad(uVolumeA, ivec3(x, y, z))) / 1000.f;

    // normalize
    if (data.a > 1)
    {
        data.xyz /= data.a;
        data.a = 1;
    }

    // repair
    if (length(data.xyz) < .01)
        data.xyz = vec3(0,0,0);

    imageStore(uOut, ivec3(x, y, z), data);
}
