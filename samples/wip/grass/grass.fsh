#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

uniform vec3 uLightDir;
uniform vec3 uCamPos;
uniform vec3 uSpecularColor;
uniform float uSpecularExponent;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vColor;


void main() {
    vec3 fColor = vColor;

    float shadow = vPosition.y;

    vec3 L = normalize(uLightDir);
    vec3 T = normalize(vTangent);
    vec3 V = normalize(vPosition - uCamPos);
    vec3 H = normalize(L + V);

    // from http://amd-dev.wpengine.netdna-cdn.com/wordpress/media/2012/10/Scheuermann_HairRendering.pdf
    //vec3 diffuse = vec3(clamp(mix(0.25, 1.0, dot(vNormal, uLightDir)), 0, 1));
    //fColor *= diffuse;

    //float diffuse = 1 - abs(dot(T, L));
    //fColor *= diffuse;

    float dotTH = dot(T, H);
    float sinTH = sqrt(1.0 - dotTH * dotTH);
    float dirAtten = smoothstep(-1.0, 0.0, dot(T, H));
    float spec = dirAtten * pow(sinTH, uSpecularExponent);

    fColor += spec * uSpecularColor;

    fColor *= shadow;

    fColor = vec3(vPosition.z * 1);

    //fColor = abs(T);
    //fColor = vColor * (0.2 + 0.8 * max(0, dot(vNormal, uLightDir)));

    outputOpaqueGeometry(fColor);
}
