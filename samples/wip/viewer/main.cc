#include <iostream>
#include <random>

#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/util/AsyncTextureLoader.hh>

#include <polymesh/algorithms/normalize.hh>
#include <polymesh/formats.hh>

#include <glow-extras/glfw/GlfwContext.hh>
#include <glow-extras/viewer/view.hh>

#include <typed-geometry/tg.hh>

const bool sCustomSkybox = false;

int main()
{
    std::string const dataPath = glow::util::pathOf(__FILE__) + "/../../../data/";

    // Create a context
    glow::glfw::GlfwContext ctx;

    if (sCustomSkybox)
    {
        // Load a skybox, init viewer
        auto const skyboxPath = dataPath + "ibl/bell_park/skybox/skybox_";
        auto skybox = glow::TextureCubeMap::createFromData(
            glow::TextureData::createFromFileCube(skyboxPath + "posx.hdr", skyboxPath + "negx.hdr", skyboxPath + "posy.hdr", skyboxPath + "negy.hdr",
                                                  skyboxPath + "posz.hdr", skyboxPath + "negz.hdr", glow::ColorSpace::sRGB));
        glow::viewer::Viewer::GlobalInit(skybox);
    }

    std::uniform_real_distribution<float> dis(0.0f, 1.0f);
    std::default_random_engine rng;

    // Load a polymesh mesh
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<glm::vec3>();
    load(dataPath + "suzanne.obj", m, pos);
    normalize(pos); // make it -1..1

    // prepare some data
    auto uv = pos.map([](glm::vec3 v) { return glm::vec2(v.x, v.y); });
    auto vdata = pos.map([](glm::vec3 v) { return v.y; });
    auto vnormals = vertex_normals_by_area(m, pos);
    auto fdata = m.faces().map([&](pm::face_handle f) { return f.vertices().avg(pos).z; });
    auto tex = glow::Texture2D::createFromFile(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);
    auto atex = glow::AsyncTextureLoader::load2D(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);
    auto fcolors = m.faces().map([&](pm::face_handle) { return glow::colors::color(dis(rng), dis(rng), dis(rng)); });
    auto vcolors = attribute(m.vertices(), glow::colors::color(1, 0, 0));
    auto edge_lengths = m.edges().map([&](pm::edge_handle e) { return edge_length(e, pos); });
    auto ptsize = m.vertices().map([&](pm::vertex_handle v) { return v.edges().avg(edge_lengths); });

    // Simplest view
    glow::viewer::view(pos);

    // Grid of examples
    {
        auto v = glow::viewer::grid();

        // ADL view
        view(pos);

        // smooth normals
        view(polygons(pos).smooth_normals());

        // Colored faces
        view(pos, fcolors);

        // Colored vertices
        view(pos, vcolors);

        // Mapped 1D vertex data
        view(pos, mapping(vdata).linear({0, 0, 0}, {1, 0, 0}, 0.1f, 0.3f));

        // Mapped 1D face data
        view(pos, mapping(fdata).linear({0, 1, 0}, {0, 0, 1}, -0.5f, 0.5f).clamped());

        // Textured mesh
        view(pos, textured(uv, tex));

        // Textured mesh (async texture)
        view(pos, textured(uv, atex));

        // Simple point cloud
        view(points(pos));

        // Square, oriented point cloud with adaptive point size
        view(points(pos).point_size_world(ptsize).normals(vnormals).square());

        // Configure view
        {
            auto v = glow::viewer::view();
            v.view(pos, glow::colors::color(0, 1, 0));
            // when out-of-scope, v immediately shows
        }

        // Multiple objects
        {
            auto v = glow::viewer::view();
            v.view(polygons(pos).move({-1.5f, 0, 0}), glow::colors::color(1, 0, 0));
            v.view(polygons(pos).move({+1.5f, 0, 0}), glow::colors::color(0, 0, 1));

            // or:
            v.view(pos, scale(glm::vec3(0.5f, 1, 1.5f)), glow::colors::color(0, 1, 0));
        }

        // Complex material
        // TODO

        // Text
        // TODO

        // Images
        // TODO

        // Scene setup
        // TODO

        // Multiple views
        {
            auto v = glow::viewer::columns();
            glow::viewer::view(pos, glow::colors::color(0, 0, 1)); // attaches to columns view
            v.view(pos, glow::colors::color(0, 0, 1));             // explicitly adds view
            {
                auto r = v.rows();
                r.view(pos, glow::colors::color(0, 1, 1));
                r.view(pos, glow::colors::color(1, 1, 0));
                // when out-of-scope, r attaches to v
            }
            // when out-of-scope, v immediately shows
        }

        // Interaction
        {
            // TODO
        }

        // Picking
        {
            // TODO
        }
    }

    // tg objects
    {
        auto v = glow::viewer::grid();
        tg::rng rng;

        view(tg::segment3(tg::pos3(0, 0, 0), tg::pos3(1, 0, 0)), tg::color3::red);
        view(tg::triangle3({0, 0, 0}, {1, 0, 0}, {0, 1, 0}), tg::color3::blue);
        view(tg::aabb3({-0.3f, 0, -0.4f}, {0.2f, 0.5f, 0.1f}), tg::color3::green);
        view(lines(tg::aabb3({-0.3f, 0, -0.4f}, {0.2f, 0.5f, 0.1f})));

        // vector versions
        {
            std::vector<tg::segment3> segs;
            for (auto i = 0; i < 20; ++i)
                segs.emplace_back(uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit));
            view(segs);
        }
        {
            std::vector<tg::triangle3> tris;
            for (auto i = 0; i < 20; ++i)
                tris.emplace_back(uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit));
            view(tris);
        }
        {
            std::vector<tg::pos3> pts;
            for (auto i = 0; i < 500; ++i)
                pts.push_back(uniform(rng, tg::sphere3::unit));
            view(pts);
        }

        {
            std::vector<tg::box3> bbs;
            for (auto i = 0; i < 4; ++i)
            {
                auto e0 = uniform_vec(rng, tg::sphere3::unit);
                auto e1 = cross(e0, uniform_vec(rng, tg::sphere3::unit));
                auto e2 = cross(e1, e0);

                auto c = uniform(rng, tg::ball3::unit);

                e0 = normalize(e0) * uniform(rng, 0.2f, 0.5f);
                e1 = normalize(e1) * uniform(rng, 0.2f, 0.5f);
                e2 = normalize(e2) * uniform(rng, 0.2f, 0.5f);
                bbs.push_back(tg::box3(c, {e0, e1, e2}));
            }

            view(lines(bbs));
            view(bbs);
        }
    }

    // Animation
    {
        // TODO
    }

    // Alternatively, output a single frame headlessly
    // auto view = glow::viewer::view(pos);
    // view->render()->bind().writeToFile("viewer_output.png");
    // view->to_file("viewer_output.png");
    // view->to_texture() -> SharedTexture2D/Rect;
    // view->to_image() -> img;
    // view->into_file(ostream&);
    // view->into_texture(SharedTexture2D/Rect);
    // view->into_image(img&);

    return 0;
}
