#include <doctest.hh>

#include <array>
#include <vector>

#include <glm/glm.hpp>
#include <typed-geometry/tg.hh>

#include <glow-extras/viewer/view.hh>

TEST_CASE("view(...) for meshes")
{
    pm::Mesh m;
    auto glm_pos = m.vertices().make_attribute<glm::vec3>();
    auto tg_pos = m.vertices().make_attribute<tg::pos3>();

    view(glm_pos);
    view(tg_pos);

    view(polygons(glm_pos));
    view(polygons(tg_pos));

    view(points(glm_pos));
    view(points(tg_pos));
}

TEST_CASE("view(...) for point clouds")
{
    using glow::viewer::view;

    std::vector<glm::vec3> glm_pts;
    std::vector<tg::pos3> tg_pts;

    view(glm_pts);
    view(tg_pts);
}
